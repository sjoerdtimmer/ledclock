-- inspiration from: http://randomnerdtutorials.com/esp8266-web-server/
srv=net.createServer(net.TCP)
srv:listen(80,function(conn)
	conn:on("receive", function(client,request)
		dofile("response.lua")
		handlerequest(client,request)
		handlerequest = nil
        client:close();
        collectgarbage();
    end)
end)