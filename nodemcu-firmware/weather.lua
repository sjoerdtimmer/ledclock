
-- NUMLEDS = 72

function interpolateweatherdata()
	local now,led,pixeltime,point1,point2,alpha
	now,_ = rtctime.get()
	-- red = {}
	-- print("scaled weather forecast")
	for led=1,72 do 
		-- print("calculating pixel:",led)
		pixeltime = now+(led-1)*3600/NUMLEDS -- each led is 50 seconds (60*60/72)
		-- print("time: ",pixeltime)
		point1 = math.floor((pixeltime-weatherstart) / 300)  -- each datapoint is 300 seconds (5 minutes)
		point2 = point1 + 1
		if point1 < 1 or point2 > 25 then
			return nil
		end
		-- print("points: ",point1,point2)
		alpha  = ((pixeltime-weatherstart) % 300) / 300      -- distance to point1, scaled to [0,1)
		-- print("alpha:",alpha)
		red[led] = alpha*weatherdata[point2] + (1-alpha)*weatherdata[point1]
		red[led] = 1.15^red[led] - 1 -- make it exponential, -1 to compensate that 1^0=1
		red[led] = math.floor(red[led])
		red[led] = math.max(red[led],0)
		red[led] = math.min(red[led],255)
		-- print(led,leddata[led])
		-- tmr.wdclr()
	end
	weatherdata = nil
end

-- blocking display function
function showweather()
	tmr.stop(0)
	tmr.delay(50000) -- for good measure
	-- blink red dot
	for i=1,5 do
		-- clear display
		ws2812.write(3,string.char(0,255,0)..string.char(0,0,0):rep(NUMLEDS-1),NUMLEDS*3)
		tmr.delay(250000)
		ws2812.write(3,string.char(0,0,0):rep(NUMLEDS),NUMLEDS*3)
		tmr.delay(250000)
	end
	-- actual weather display
	-- bytes = {}
	for p=1,NUMLEDS do
		for i=1,NUMLEDS do
			if i==1 then
				bytes[3*ROTATE(i)-1] = 255 -- red
				bytes[3*ROTATE(i)-2] = 0   -- green
				bytes[3*ROTATE(i)  ] = 0   -- blue
			elseif i<p+1 then
				bytes[3*ROTATE(i)-1] = 0 --255 - leddata[i]
				bytes[3*ROTATE(i)-2] = 0 --255 - leddata[i]
				bytes[3*ROTATE(i)  ] = red[i]
			elseif i==p+1 then     -- red dot
				bytes[3*ROTATE(i)-1] = 255
				bytes[3*ROTATE(i)-2] = 0 
				bytes[3*ROTATE(i)  ] = 0
			else              -- hidden part
				bytes[3*ROTATE(i)-1] = 0
				bytes[3*ROTATE(i)-2] = 0
				bytes[3*ROTATE(i)  ] = 0
			end
		end
		rgb = string.char(unpack(bytes))
		ws2812.write(3,rgb,NUMLEDS*3)
		tmr.delay(10000)
	end
	-- tmr.delay(5000000)
	-- ws2812.write(3,string.char(0,0,0):rep(NUMLEDS),NUMLEDS*3)

	-- give the other garbage collector some time to clean up, 
	-- while we continue to display the weather
	tmr.alarm(0,5000,0,clock)
end


local conn = net.createConnection(net.TCP, 0)

conn:on("receive", function(conn, payload) 
	-- print("got data: \""..payload.."\"") 
	local i,j
	i,j = string.find(payload,"{") 
	if i ~= nil then
		local json = string.sub(payload,i)
		-- print("raw json:")
		-- print(json)
		local data = cjson.decode(json)
		-- print("start: ",data['start'])
		-- print("data points:")
		-- for k,v in pairs(data['precip']) do
			-- print(k,v)
		-- end
		weatherstart = data['start']
		weatherdata  = data['precip']
		interpolateweatherdata()
		local sum = 0
		for i=1,NUMLEDS do
			-- print("weather",i,red[i])
			sum = sum+red[i]
		end
		if sum > 0 then
			-- an optional memery saver would be to put showweather in a separate file
			-- the clock can be stopped before reading it, creating some extra space
			showweather()
		end
		interpolateweatherdata = nil
		showweather = nil
		conn:close()
		con = nil
		collectgarbage()
		-- clock()
	-- else
	-- 	print("data is not json ;(")
	end

end)

conn:on("connection",function(conn)
	print("tcp connected!")
	conn:send("GET /app/forecast.php?type=json&x=366&y=429 HTTP/1.1\r\n" 
	.."Host: buienalarm.nl\r\n"
	.."\r\n",function()
		print("trasnfer done!")
	end)
end)

-- conn:on("disconnection", function(conn)
--     print("connection closed")
-- end)

conn:connect(80,'188.226.199.6')
-- conn:connect(80,'192.168.2.3') -- for local debugging


