-- todo: implement DST auto detect:
-- http://stackoverflow.com/questions/5590429/calculating-daylight-savings-time-from-only-date
-- which will require getting the year/month/day/dow: http://stackoverflow.com/questions/21593692/convert-unix-timestamp-to-date-without-system-libs


function ROTATE(led)
	led = led-1 -- range [0,71]
	led = (2*NUMLEDS-led+7)%NUMLEDS
	led = led+1 -- range [1,72]
	return led
end

function on_wifi_up()
	print("wifi up! getting the time...")
	--may fail, hence the call to self on error
	sntp.sync('91.148.192.49',on_ntp_up,on_wifi_up)
	-- TODO: better idea: get local time from:
	-- www.timeapi.org/cet/now
	-- retuns: 2016-02-18T14:27:45+01:00
	-- which should be easy to parse
end


function on_ntp_up()
	print("NTP up and running!")
end

function init()
	NUMLEDS = 72
	ws2812.write(3,string.char(0,0,0):rep(NUMLEDS),3*NUMLEDS)

	dofile("hsv.lua")
	dofile("utils.lua")
	dofile("clock.lua")
	dofile("web.lua")

	if wifi.getmode() ~= wifi.STATIONAP then
		cfg={}
		cfg.ssid="LedClock"
		cfg.pwd="NaeVaep3fu"
		wifi.ap.config(cfg)
		-- cfg= nil
	end

	wifi.sta.autoconnect(0)
	wifi.sta.disconnect()
	wifi.setmode(wifi.STATIONAP)

	wifi.sta.eventMonReg(wifi.STA_GOTIP, on_wifi_up)
	wifi.sta.eventMonStart()
	-- connect using stored credentials
	wifi.sta.connect()



	rot = 0
	value = 0
	bytes = {}
	red   = {}
	green = {}
	blue  = {}

	timed_rainbow()

end


function timed_rainbow()
	-- bytes = {}
	value = math.min(1,value + 0.05)
	for j=1,NUMLEDS do
		bytes[j*3-1],bytes[j*3-2],bytes[j*3] = hsvToRgb(((j-1)/NUMLEDS-rot+1)%1,1,value)
	end
	ws2812.write(3,string.char(unpack(bytes)),3*NUMLEDS)
	rot = rot + 2/NUMLEDS
	-- reschedule if not done yet:
	if fst(rtctime.get())==0 or tmr.now() < 5000000 then
		tmr.alarm(6,10,0,timed_rainbow)
	else
		fadeout_rainbow()
	end
end

function fadeout_rainbow()
	while value >= 1/20 do
		value = value-1/20
		-- bytes = {}
		for j=1,NUMLEDS do
			bytes[j*3-1],bytes[j*3-2],bytes[j*3] = hsvToRgb(((j-1)/NUMLEDS-rot+1)%1,1,value)
			-- print("hsv:",((j-1)/NUMLEDS-rot+1)%1,1,value)
			tmr.wdclr()
		end
		-- print("rgbdata:",unpack(bytes))
		ws2812.write(3,string.char(unpack(bytes)),3*NUMLEDS)
		tmr.delay(1000)
		rot = rot + 2/NUMLEDS
	end
	ws2812.write(3,string.char(0,0,0):rep(NUMLEDS),3*NUMLEDS)
	-- start the clock:
	rot = nil
	value = nil
	clock()
	-- tmr.alarm(1,300000,1,function() dofile("weather.lua") end) -- 300000 is 5 minutes
	timed_rainbow = nil
	on_wifi_up = nil
	on_ntp_up = nil
	fadeout_rainbow = nil
	init = nil
	collectgarbage();
end


function clock()
	display_time()
	tmr.alarm(0,10,0,clock)
end

init() 

