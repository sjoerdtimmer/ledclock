<?php 

function cURLcheckBasicFunctions() 
{ 
  if( !function_exists("curl_init") && 
      !function_exists("curl_setopt") && 
      !function_exists("curl_exec") && 
      !function_exists("curl_close") ) return false; 
  else return true; 
} 

function cURLdownload($url) 
{ 
  if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions"; 
  $ch = curl_init(); 
  if($ch)  { 
    if( !curl_setopt($ch, CURLOPT_URL, $url) ) { 
      curl_close($ch); // to match curl_init() 
      return "FAIL: curl_setopt(CURLOPT_URL)"; 
    } 
    //if( !curl_setopt($ch, CURLOPT_FILE, $fp) ) return "FAIL: curl_setopt(CURLOPT_FILE)"; 
    if( !curl_setopt($ch, CURLOPT_HEADER, 0) ) return "FAIL: curl_setopt(CURLOPT_HEADER)"; 
    if( !curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ) return "FAIL: curl_setopt(CURLOPT_RETURNTRANSFER)"; 
    if( !curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36") ) return "FAIL: curl_setopt(CURLOPT_USERAGENT)"; 

    return curl_exec($ch) ; 
    curl_close($ch); 
  } 
  else return "FAIL: curl_init()"; 
} 

function interpolatePrecipForTime($json,$time){ 
  // the index in the array just before (or at) the given time:
  $offset = floor(($time-$json->start)/60/5); // integer part
  //$extra = ($time-$json->start)/60/5 - $offset; // floating part

  $time1 = $json->start + 5*60*$offset; // the time at $offset
  $time2 = $json->start + 5*60*($offset+1); // the time at $offset+1
  
 
  // precipitations before and after $time:
  $precip1 = $json->precip[$offset];  
  $precip2 = $json->precip[$offset+1];  


  //echo "($time1-$time-$time2)[$offset]($precip1:$precip2)"; 
  
// return the linear interpolation:
  return ($time-$time1)*$precip2/(5*60) + ($time2-$time)*$precip1/(5*60);
}


// hardcoded randomdata location:
$raw = cURLdownload("http://buienalarm.nl/app/forecast.php?type=json&x=366&y=429");
$json = json_decode($raw);

//echo time();
//print_r($json);

//echo "\n\n\n";

// the exact time corresponding to the next pixel
// we round up to the first multiple of 50
// because 50 seconds is exactly one led
$firsttime = ceil(time()/50)*50; 

//echo $firsttime;
//echo "\n\n";

// the number of that pixel
$firstpixel = ($firsttime/50)%72;

//echo $firstpixel;
//echo "\n\n";
echo pack('C',$firstpixel);
for($i=$firstpixel;$i<$firstpixel+72;$i++){
  // $i is the pixel number
  // $t is the exact time at that pixel 
  $t = $firsttime+($i-$firstpixel)*50; 
  // $v is the interpolated precipitation at that pixel
  $v = interpolatePrecipForTime($json,$t);
  //echo pack('C',$i);
  echo pack('C',min($v,255));
//  echo "$i $t $v\n";
}

