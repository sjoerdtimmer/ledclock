
#include <TimerOne.h>




byte x=0;
#define MILLIINTERVAL 10// every 10ms
#define MAX 2400

void setup(){
	Serial.begin(115200);

	Timer1.initialize(1000*MILLIINTERVAL);  // in mucros
  	Timer1.attachInterrupt(tick); 
}

void loop() {
	Serial.print("t0=");
	Serial.print(millis());
	Serial.print("    t1=");
	Serial.println(myMillis());
	//delay(800);

	long i=0;

	// this takes approx. 2ms which means that Timer/Counter0 will miss 1 or 2 interrupts during this loop
	// IRS is called only once so some millis get lost and millis() will walk behind slightly:
	noInterrupts();
	for(;i<MAX;i++){
		x += i;
	}
	interrupts();
}






unsigned long myMillisCounter = 0;

void tick(){
	myMillisCounter+=MILLIINTERVAL;
}

unsigned long myMillis(){
	// to make sure a write is not spoiling the read we make an uninterrupted copy
	noInterrupts();
	unsigned long mymillicopy = myMillisCounter;
	interrupts();
	return mymillicopy;
}


// mytimer started at:
/// time     millis()   myMillis()  correct-value:
// 14:42:12  0          0
// 14:47:12  281681     299888      300000