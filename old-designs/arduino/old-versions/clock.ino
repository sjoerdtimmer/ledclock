 
#include <Adafruit_NeoPixel.h>
#include <Time.h> 
#include <UIPEthernet.h>
#include <UIPUdp.h>
#include <SPI.h>
#include <SimpleTimer.h>
#include <Timezone.h> 

// ledstrip: 
#define LEDPIN A3
Adafruit_NeoPixel strip = Adafruit_NeoPixel(72, LEDPIN, NEO_GRB + NEO_KHZ800);

#define ROTATE(x) (-x+18+72)%72 // rotated 90' and reversed on a 72 led strip

// network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; 
// NTP Servers:
IPAddress timeServer(132, 163, 4, 101); // time-a.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 102); // time-b.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 103); // time-c.timefreq.bldrdoc.gov
EthernetUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets




// timezone setup:
//US Eastern Time Zone (New York, Detroit)
TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120};    //Daylight time = UTC +2 hours
TimeChangeRule CET = {"CET", Last, Sun, Oct, 3, 60};     //Standard time = UTC +1 hours
Timezone myTZ(CEST, CET);

time_t local;
#define millidelay 50
//time_t prevDisplay = 0; // when the digital clock was displayed

SimpleTimer timer = SimpleTimer();
boolean firstTimeUpdate = true;



void setup() 
{
	strip.begin();
	strip.show(); // Initialize all pixels to 'off'
	// strip.setBrightness(20);

	// we are going to fake a progressbar ;)
 	// it will actually hang during dhcp
 	for(int i=0;i<0.64*strip.numPixels();i++){
 		strip.setPixelColor(ROTATE(i),0,0,255);
 		strip.show();
 		delay(15);
 	}
	if (Ethernet.begin(mac) == 0) {
	    // it failed!
	    for(int i=0;i<strip.numPixels();i++){ // permanent red!
 			strip.setPixelColor(ROTATE(i),255,0,0);
 		}
 		strip.show();
 		delay(5000);
 		// try to reset:
 		asm volatile ("  jmp 0"); 
	}
	// we have a connection!
	for(int i=0.64*strip.numPixels();i<strip.numPixels();i++){
 		strip.setPixelColor(ROTATE(i),0,0,255);
 		strip.show();
 		delay(5);
 	}

	Udp.begin(localPort);
	setSyncProvider(getNtpTime);
	//setTime(1406837469);

	timer.setInterval(millidelay,ledClockDisplay);
}


void loop() {  
	if(firstTimeUpdate) return; // don't start showing the clock until we have a time
	if (timeStatus() == timeNotSet) return;
	timer.run();
	delay(100);
}



// int lastsec = 0;
// int millissincelastsec = 0;

void ledClockDisplay() {
	local = myTZ.toLocal(now());
	uint8_t hours = hour(local)%12;

	// clear displsy
	for (int i=0;i < strip.numPixels() ; i++) { 
		strip.setPixelColor(i,0);
	}

	// set the hours:

	//  // method 1: fill it up:
	// for (int i=0;i < strip.numPixels() ; i++) { 
	// 	// every hour is 5 pixels, every 12 minute is one extra pixel if we had 60 leds
	// 	// we have 72, so every hour is 6 leds, every 10 minutes is one extra pixel
	// 	int hpix = hours*6+minute()/10;
	// 	if(i < hpix-2  || (i<hpix-1 && hpix < 10) || (i<hpix && hpix < 5)){ 
	// 		strip.setPixelColor(ROTATE(i),0,255,0);
	// 	}else{
	// 		strip.setPixelColor(ROTATE(i),0,0,0);
	// 	}
	// }

  // method 2: 1 pixel per hour:
	// for(int i=0;i<=hours;i++){
	// 	strip.setPixelColor(ROTATE((i*6)),0,255,0);
	// }



	// fill seconds in green
	for (int i=0;i < strip.numPixels() ; i++) { 
		if((i<second()*6/5) != (minute()%2==0)){ 
			strip.setPixelColor(ROTATE(i),0,255,0);
		}else{
			strip.setPixelColor(ROTATE(i),0,0,0);
		}
	}


	// set a hour indicator in red:
	int h = hours*6+minute()/10;
	// strip.setPixelColor(ROTATE((s-3)),0,0,0);
	// strip.setPixelColor(ROTATE((s-2)),0,0,0);
	strip.setPixelColor(ROTATE((h-1)),0,0,0);
	strip.setPixelColor(ROTATE(h)    ,255,0,0);
	strip.setPixelColor(ROTATE((h+1)),0,0,0);
	// strip.setPixelColor(ROTATE((s+2)),0,0,0);
	// strip.setPixelColor(ROTATE((s+3)),0,0,0);


	// set a minute indicator in blue:
	int m = minute()*6/5;
	// strip.setPixelColor(ROTATE((m-3)),0,0,0);
	// strip.setPixelColor(ROTATE((m-2)),0,0,0);
	strip.setPixelColor(ROTATE((m-1)),0,0,0);
	strip.setPixelColor(ROTATE(m)    ,0,0,255);
	strip.setPixelColor(ROTATE((m+1)),0,0,0);
	// strip.setPixelColor(ROTATE((m+2)),0,0,0);
	// strip.setPixelColor(ROTATE((m+3)),0,0,0);

	// // set a second indicator in red:
	// int s = second()*6/5;
	// // strip.setPixelColor(ROTATE((s-3)),0,0,0);
	// // strip.setPixelColor(ROTATE((s-2)),0,0,0);
	// strip.setPixelColor(ROTATE((s-1)),0,0,0);
	// strip.setPixelColor(ROTATE(s)    ,255,0,0);
	// strip.setPixelColor(ROTATE((s+1)),0,0,0);
	// // strip.setPixelColor(ROTATE((s+2)),0,0,0);
	// // strip.setPixelColor(ROTATE((s+3)),0,0,0);

	
	strip.show();

	// millissincelastsec = (millissincelastsec+millidelay+1) % 1000;
	// lastsec = second();
}


double circDist(double x1,double x2){
	if(x2 < x1){ // make sure x2 > x1
		double tmp = x1;
		x1=x2;
		x2=tmp;
	}
	if(x2-x1 < 0.5) return x2-x1;
	return x1 - x2 + 1; // +1 because otherwise we get the negative distance
}





/*
TODO:
Opmerkingen:
code verbeter ideeen:
* smoothing
* weather

3d model verbeterideen:
* infill is zichtbaar:)
* spijltjes?
* lensjes?
* passen de delen wel op elkaar? nee :s
+ plek voor kabel uit (ook nadenken over hoe ophangen)
+ condensator past niet!
* rand sowiso transparant maken? dan kan het ook weer als 1 ding geprint worden...

ook belangrijk:
* hoe ophangen? plaat erachter?
* case voor electronics

*/








/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets


time_t getNtpTime() {
	double progress = 0.2;
	if(firstTimeUpdate){
		for(int i=0;i<progress*strip.numPixels();i++){
			strip.setPixelColor(ROTATE(i),0,255,0);
			strip.show();
			delay(15);
		}
	}

	while (Udp.parsePacket() > 0) ; // discard any previously received packets
	// Serial.println("Transmit NTP Request");
	sendNTPpacket(timeServer);
	uint32_t beginWait = millis();
	while (millis() - beginWait < 1500) {
		if(firstTimeUpdate){
			progress = 0.2+0.8*(millis()-beginWait)/1500;
			for(int i=0;i<progress*strip.numPixels();i++){
				strip.setPixelColor(ROTATE(i),0,255,0);
				strip.show();
				delay(15);
			}
		}
		int size = Udp.parsePacket();
		if (size >= NTP_PACKET_SIZE) {
			// Serial.println("Receive NTP Response");
			Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
			unsigned long secsSince1900;
			// convert four bytes starting at location 40 to a long integer
			secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
			secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
			secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
			secsSince1900 |= (unsigned long)packetBuffer[43];
			if(firstTimeUpdate){
				for(int i=0;i<strip.numPixels();i++){
					strip.setPixelColor(ROTATE(i),0,255,0);
					strip.show();
					delay(15);
				}
				firstTimeUpdate = false;
			}
			return secsSince1900 - 2208988800UL; // to obtion secsSince1970
		}
	}
	// Serial.println("No NTP Response :-(");
	if(firstTimeUpdate){
	    for(int j=0;j<5;j++){ // blink red 5 times
		    for(int i=0;i<strip.numPixels();i++){
	 			strip.setPixelColor(ROTATE(i),255,0,0);
	 		}
	 		strip.show();
	 		delay(500);
	 		for(int i=0;i<strip.numPixels();i++){
	 			strip.setPixelColor(ROTATE(i),0,0,0);
	 		}
	 		strip.show();
	 		delay(500);
	 	}
	}
	// something went wrong getting the time: try to reboot
	asm volatile ("  jmp 0"); 
	//return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;
	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:                 
	Udp.beginPacket(address, 123); //NTP requests are to port 123
	Udp.write(packetBuffer, NTP_PACKET_SIZE);
	Udp.endPacket();
}

