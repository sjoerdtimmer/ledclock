 
#include <Adafruit_NeoPixel.h>
#include <Time.h> 
#include <UIPEthernet.h>
#include <UIPUdp.h>
#include <SPI.h>
#include <Timezone.h> 
#include <SoftwareSerial.h>
#include <TimerOne.h>

// ledstrip: 
#define LEDPIN A3
Adafruit_NeoPixel strip = Adafruit_NeoPixel(72, LEDPIN, NEO_GRB + NEO_KHZ800);
#define ROTATE(x) (-(x)+9+72)%72 // rotated 90' and reversed on a 72 led strip


// network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; 
// NTP stuff:
// IPAddress timeServer(192, 168, 16, 1); // time-a.timefreq.bldrdoc.gov
IPAddress timeServer(132, 163, 4, 101); // time-a.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 102); // time-b.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 103); // time-c.timefreq.bldrdoc.gov
EthernetUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
// http stuff:
IPAddress weatherServer(131,211,32,72);
UIPClientExt client;

// my own constants
#define NTP_UPDATE_INTERVAL 60*60 
// #define NTP_UPDATE_INTERVAL 10 
#define WEATHER_UPDATE_INTERVAL 300

#define MYTIMEINTERVAL 10// update myMillis() every 10ms

#define ERROR_DHCP_FAILED 2
#define ERROR_NTP_TIMEOUT 3
#define ERROR_HTTP_TRANSMISSION_TIMEOUT 4
#define ERROR_HTTP_FAILED_TO_START 5
#define ERROR_HTTP_CONNECT_TIMEOUT 6
#define ERROR_HTTP_BADDATA 7


#define LEDPIN_GND 6
#define LEDPIN_ORANGE 7
#define LEDPIN_BLUE 8



// #define SERIALDEBUG
#define WITHWEATHER


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// timezone setup:
//US Eastern Time Zone (New York, Detroit)
TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120};    //Daylight time = UTC +2 hours
TimeChangeRule CET = {"CET", Last, Sun, Oct, 3, 60};     //Standard time = UTC +1 hours
Timezone myTZ(CEST, CET);
time_t local;







#ifdef SERIALDEBUG
SoftwareSerial debug(A2, A1); // RX, TX
#endif



// a note on timing
// millis() uses an internal interrupt that fires once every millisecond and increments an internal counter.
// due to interrupt-free zones in the ledstrip library it can occur that more than one of these triggers is missed and a ms gets lost.
// Normally when an interrupt arrives during an interrupt free block of code, the handler get called immediately after that block.
// the problem is that, if after the first interrupt another interrupt arrives the handler is called only once.
// The fix is easy: increment the delay between interrupt to 10ms and advance the millisecond timer by 10 every interrupt. 
// this makes sure that never more than one interrupt is missed. 
// I used Timer1 with the tickMyMillis callback and myMillisCounter counter to emulate the code of millis() with this added patch.


void setup() 
{
	Timer1.initialize(1000*MYTIMEINTERVAL);  // in mucros
  	Timer1.attachInterrupt(tickMyMillis); 

	strip.begin();
	strip.show(); // Initialize all pixels to 'off'
	// strip.setBrightness(20); // easy on the eyes while debugging

	// initialise the status led:
	pinMode(LEDPIN_GND,OUTPUT);
	pinMode(LEDPIN_BLUE,OUTPUT);
	pinMode(LEDPIN_ORANGE,OUTPUT);
	// initially all are off
	digitalWrite(LEDPIN_GND, LOW); // meaning that it will source current and act as a ground pin
	digitalWrite(LEDPIN_BLUE, LOW);
	digitalWrite(LEDPIN_ORANGE, HIGH); // we start in orange


	#ifdef SERIALDEBUG
	// create an additional ground at A0, used for serial debugger
	pinMode(A0, OUTPUT);
	digitalWrite(A0, LOW);

	debug.begin(4800);
	debug.println("welcome");
	#endif

	getIP(); // includes visualisation but blocks progress

	digitalWrite(LEDPIN_ORANGE, LOW);
	digitalWrite(LEDPIN_BLUE, HIGH);

	// request our first ntp update
	sendNtpRequest(timeServer);
}


unsigned long myMillisCounter = 0;

void tickMyMillis(){
	myMillisCounter+=MYTIMEINTERVAL;
}

unsigned long myMillis(){
	// to make sure a write is not spoiling the read we make an uninterrupted copy
	noInterrupts();
	unsigned long mymillicopy = myMillisCounter;
	interrupts();
	return mymillicopy;
}

// here are some patched timekeeping function to use myMillis() instead of millis()
static uint32_t sysTime = 0;
static uint32_t prevMillis = 0;
time_t myNow() {
  while (myMillis() - prevMillis >= 1000){      
    sysTime++;
    prevMillis += 1000;	
  }
  return (time_t)sysTime;
}

void mySetTime(time_t t) { 
  sysTime = (uint32_t)t;  
  prevMillis = myMillis();
} 



time_t lastWeatherRequest; // in seconds
time_t lastWeatherUpdate; // in seconds
// there is a tiny trick in the following two lines
// we manually do the first ntp request so this ensures that the "waiting for ntp response state" is immediately active
time_t lastNtpUpdate=0; // in seconds
time_t lastNtpRequest=1; // in seconds
time_t lastWholeTimeSeconds; // necessary to determine the milliseconds into the second
time_t lastWholeTimeMillis;

void loop(){
	if(lastNtpUpdate < lastNtpRequest){ // a request has been sent out but no answer has been recorde yet!
		if(myNow()%2){// blink
			digitalWrite(LEDPIN_ORANGE, HIGH);
			digitalWrite(LEDPIN_BLUE, LOW);
		}else{
			digitalWrite(LEDPIN_ORANGE, LOW);
			digitalWrite(LEDPIN_BLUE, HIGH);
		}
	}

	// check for ntp update:
	if(checkForNtpUpdate()) 
		lastNtpUpdate = myNow();
	// check for NTP timeouts: after 3 transmits without receipt
	else if(myNow() - lastNtpUpdate > 3.5 * NTP_UPDATE_INTERVAL) 
		fatal_error(ERROR_NTP_TIMEOUT);

	#ifdef WITHWEATHER
	// check for weather update:
	if(checkForWeatherUpdate()) 
		lastWeatherUpdate = myNow();
	#endif


	if(lastWholeTimeSeconds != myNow()){ // a new second has arrived!
		lastWholeTimeMillis = myMillis();
		lastWholeTimeSeconds = myNow();
	}
	displayTime(lastWholeTimeSeconds, myMillis()-lastWholeTimeMillis);


	if( myNow() - lastNtpRequest >= NTP_UPDATE_INTERVAL ) {
		sendNtpRequest(timeServer);
		lastNtpRequest = myNow();
	}

	#ifdef WITHWEATHER
	// request weather update 
	if( myNow()%WEATHER_UPDATE_INTERVAL==0 && myNow() - lastWeatherRequest >= WEATHER_UPDATE_INTERVAL/2 ) { 
		// this triggers when the time is a multiple of WEATHER_UPDATE_INTERVAL(=5 mins)
		// but only when the last weatherdisplay was long ago to prevent it going of in  every loop() for a whole second
		// show the weather at whole intervals only
		sendWeatherRequest();
		lastWeatherRequest = myNow();
	}
	#endif
}





void displayTime(time_t time, time_t millioffset){
	
	local = myTZ.toLocal(time);
	uint8_t hours = hour(local)%12;

	#ifdef SERIALDEBUG
	debug.print(minute(time),DEC);
	debug.print(":");
	debug.print(second(time),DEC);
	debug.print("+");
	debug.print(millioffset,DEC);
	debug.print("ms (millis=");
	debug.print(myMillis(),DEC);
	debug.println(")");
	#endif

	// clear display
	for (int i=0;i < strip.numPixels() ; i++) { 
		strip.setPixelColor(i,0);
	}

	// fill seconds in green
	// I chose for all longs so that any calculation is done with longs as well because many of
	// the operations below here have large intermediate values:
	unsigned long millisIntoMinute = 1000UL*second(time)+millioffset;
	unsigned long numWholePixels = millisIntoMinute*6/5000; // 5000 millis is 6 pixels, integer division floors
	unsigned long millisIntoNextPixel = millisIntoMinute - numWholePixels*5000/6; // remaining millis after the last whole pixel
	// % operator won't work here because of rounding of the (5000/6) part!
	#ifdef SERIALDEBUG
	debug.print(millisIntoMinute,DEC);
	debug.print("ms == ");
	debug.print(numWholePixels,DEC);
	debug.print("px + ");
	debug.println(millisIntoNextPixel,DEC);
	#endif
	for (int i=0;i < strip.numPixels() ; i++) { 
		if((i<numWholePixels) != (minute(time)%2==0)){ 
			strip.setPixelColor(ROTATE(i),0,255,0);
		}else{
			strip.setPixelColor(ROTATE(i),0,0,0);
		}
	}
	// set the smoothing pixel:
	// every 5000ms equals 6 pixels, so every (5000/6)ms quals one pixel
	
	if(minute(time)%2 == 0){
		double lvl = sqrt((double)millisIntoNextPixel*6.0/5000.0);
		strip.setPixelColor(ROTATE(numWholePixels),0,(int)(255.0-255.0*lvl),0);
	}else{
		double lvl = sqrt(1.0-(double)millisIntoNextPixel*6.0/5000.0);
		strip.setPixelColor(ROTATE(numWholePixels),0,(int)(255.0-lvl*255.0),0);
	}
	


	// set a hour indicator in red:
	int h = hours*6+minute(time)/10;
	// strip.setPixelColor(ROTATE((s-3)),0,0,0);
	// strip.setPixelColor(ROTATE((s-2)),0,0,0);
	strip.setPixelColor(ROTATE((h-1)),0,0,0);
	strip.setPixelColor(ROTATE(h)    ,255,0,0);
	strip.setPixelColor(ROTATE((h+1)),0,0,0);
	// strip.setPixelColor(ROTATE((s+2)),0,0,0);
	// strip.setPixelColor(ROTATE((s+3)),0,0,0);

	// set a minute indicator in blue:
	int m = minute(time)*6/5;
	// strip.setPixelColor(ROTATE((m-3)),0,0,0);
	// strip.setPixelColor(ROTATE((m-2)),0,0,0);
	strip.setPixelColor(ROTATE((m-1)),0,0,0);
	strip.setPixelColor(ROTATE(m)    ,0,0,255);
	strip.setPixelColor(ROTATE((m+1)),0,0,0);
	// strip.setPixelColor(ROTATE((m+2)),0,0,0);
	// strip.setPixelColor(ROTATE((m+3)),0,0,0);

	strip.show();
}






#define STATUS_READY 0
#define STATUS_CONNECTING 1
#define STATUS_WAITING 2

#ifdef WITHWEATHER
byte http_status = STATUS_READY;
unsigned char weatherBuffer[72]; 

void sendWeatherRequest(){
	if(http_status != STATUS_READY) // previous request still running?
		fatal_error(ERROR_HTTP_TRANSMISSION_TIMEOUT);
	if(!client.connectNB(weatherServer, 80)) fatal_error(ERROR_HTTP_FAILED_TO_START);
	// debug.println("connecting...");
	http_status = STATUS_CONNECTING;
}

boolean checkForWeatherUpdate(){
	if(http_status == STATUS_CONNECTING){
		if(client.connected()){ // let's send a http request
			#ifdef SERIALDEBUG
			debug.println("ESTABLISHED!");
			#endif
			
			// client.flush();
			client.println("GET /~3118479/weather.php HTTP/1.1");
			client.println("Host: www.staff.science.uu.nl");
			//client.println("User-Agent: Mozilla/5.0");
			client.println("Connection: close");
			client.println();
			// now we only have to wait for the response:
			http_status = STATUS_WAITING;
		}else if(!client){ // boolean value of client will be true as long as connecting is forthcoming
			fatal_error(ERROR_HTTP_CONNECT_TIMEOUT);
		}
	}
	if(http_status == STATUS_WAITING ){
		if(client.available() > 0){
			consumeHTTPHeader();
			
			byte startpoint = client.read();
			if(startpoint == 0xff){ // special value to indicate all 0's
				client.stop();
				http_status = STATUS_READY;
				return true;
			}

			if(client.available() < 72){
				fatal_error(ERROR_HTTP_BADDATA);
			}
			
			#ifdef SERIALDEBUG
			debug.println("weather display...");
			#endif
			
			client.read(weatherBuffer, 72);
			client.stop(); // disconnect

			// // clear the display
			for(int i=0;i<72;i++){ 
				strip.setPixelColor(ROTATE(i),0,0,0);
			}
			strip.show();
			// int m = minute(myNow())*6/5; 
			
			for(int i=0;i<5;i++){
				strip.setPixelColor(ROTATE(startpoint)    ,255,0,0);
				strip.show();
				delay(250);
				strip.setPixelColor(ROTATE(startpoint)    ,0,0,0);
				strip.show();
				delay(250);
			}

			for(int i=0;i<72;i++){ 
				strip.setPixelColor(ROTATE((i+startpoint)),0,0,weatherBuffer[i]);
				strip.setPixelColor(ROTATE((i+startpoint+1)),MAX(127,weatherBuffer[i]),0,0);
				strip.show();
				delay(50);
			}

			strip.setPixelColor(ROTATE((71+startpoint+1)),255,0,0);

			delay(8000);
			#ifdef SERIALDEBUG
			debug.println("done");
			#endif

			http_status = STATUS_READY; // done	
			return true;
		} 
	}
	
	return false;
}




void consumeHTTPHeader(){
	int prev = 0;
	int next;

	while(client.available() > 0){
		next = client.read();
		#ifdef SERIALDEBUG
		debug.print("discarding ");
		debug.println(next,DEC);
		#endif
		/* The content starts after two consecutive cr-lf pairs:
		so that would look like:13 10 13 10 in bytes
		so if we see a 13 immediately after a 10 we know we are there!
		*/
		if(next==13 && prev==10){ 
			client.read();// consumes the last 10
			return;
		}
		prev = next;
	}
	// if we reach this point there never was a message??
	fatal_error(ERROR_HTTP_BADDATA);
}

#endif




const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

/* check for ntp packets and set the time, returns true if there was an update */
boolean checkForNtpUpdate(){
	if (Udp.parsePacket() >= NTP_PACKET_SIZE) { // non-blocking, returns 0 if nothing available
		// #ifdef SERIALDEBUG
		// debug.println("update ready...");
		// #endif
		Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
		unsigned long secsSince1900;
		// convert four bytes starting at location 40 to a long integer
		secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
		secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
		secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
		secsSince1900 |= (unsigned long)packetBuffer[43];
		mySetTime(secsSince1900 - 2208988800UL); // to obtain secsSince1970
		#ifdef SERIALDEBUG
		debug.println("time updated!");
		#endif
		return true;
	}
	return false;
}

// send an NTP request to the time server at the given address
void sendNtpRequest(IPAddress &address) {	
	// discard any old packets:

	while (Udp.parsePacket() > 0) ; 

	// #ifdef SERIALDEBUG
	// debug.println("sending NTP request...");
	// #endif

	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;
	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:                 
	Udp.beginPacket(address, 123); //NTP requests are to port 123
	Udp.write(packetBuffer, NTP_PACKET_SIZE);
	Udp.endPacket();

	// #ifdef SERIALDEBUG
	// debug.println("ntp request done!");
	// #endif
}









/* DHCP stuff: */

// byte dhcp_progress = 0;

#define DHCP_FRACTION 0.25
void getIP(){
	// try to connect:
	if (Ethernet.begin(mac) == 0) {
	    // it failed!
	    fatal_error(ERROR_DHCP_FAILED);
	}
	// visual feedback of success:
	// for(int i=0.64*strip.numPixels();i<strip.numPixels();i++){
 // 		strip.setPixelColor(ROTATE(i),0,0,255);
 // 		strip.show();
 // 		delay(5);
 // 		#ifdef SERIALDEBUG
 // 		debug.println(".");
 // 		#endif
 // 	}
 	#ifdef SERIALDEBUG
 	debug.println("dhcp: OK");
 	#endif
}









void setAllPixels(int r,int g, int b){
	for(int p=0;p<strip.numPixels();p++){
		strip.setPixelColor(p,r,g,b);
	}
	strip.show();
}



/* show an error message before rebooting */

void fatal_error(byte reason){
	#ifdef SERIALDEBUG
	debug.print("FATAL ERROR: ");
	debug.println(reason,DEC);
	#endif
	digitalWrite(LEDPIN_BLUE, LOW);
	digitalWrite(LEDPIN_ORANGE, LOW);

	// pulse out the error code 3 times:
	for(int repetition=0;repetition<3;repetition++){
		for(int i=0;i<reason;i++){
			digitalWrite(LEDPIN_ORANGE, HIGH);
			setAllPixels(255,0,0);
			delay(500);
			digitalWrite(LEDPIN_ORANGE, LOW);
			setAllPixels(0,0,0);
			delay(500);
		}
		delay(3000);
	}
	// soft reset
	asm volatile ("  jmp 0"); 
}

