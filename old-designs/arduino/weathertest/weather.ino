/*
	Web client
 
 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe, based on work by Adrian McEwen
 
 */

#include <SPI.h>
#include <UIPEthernet.h>
#include <Time.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };


// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void setup() {
 // Open serial communications and wait for port to open:
	Serial.begin(9600);

	// start the Ethernet connection:
	if (Ethernet.begin(mac) == 0) {
		Serial.println("Failed to configure Ethernet using DHCP");
		// no point in carrying on, so do nothing forevermore:
		// try to congifure using IP address instead of DHCP:
		return;
	}
	Serial.print("IP number assigned by DHCP is ");
	Serial.println(Ethernet.localIP());
	// give the Ethernet shield a second to initialize:
	delay(1000);
	Serial.println("connecting...");

	// if you get a connection, report back via serial:
	if (client.connect("buienalarm.nl", 80)) {
		Serial.println("connected");
		// Make a HTTP request:
		client.println("GET /app/forecast.php?type=json&x=366&y=429 HTTP/1.1");
		client.println("Host: buienalarm.nl");
		//client.println("User-Agent: Mozilla/5.0");
		client.println("Connection: close");
		client.println();

		while(client.connected()){
			// if there are incoming bytes available 
			// from the server, read them and print them:
			int phase = 0;
			time_t time = 0;
			int precip[12];

			while(client.available()) {
				char c = client.read();
				
				switch(phase){
					case 0: if(c=='{') phase=1; break;// waiting for '{'
					case 1: if(c==':') phase=2; break;// waiting for ':'
					case 2: // reading numbers until ','
						if(c==','){ phase=3; break; }
						time *= 10;
						time += c - 48; // 48=='0';
						break;
					case 3: if(c=='[') phase=4; break;// waiting for '['
					case 4:
						precip[] = 
					break;
				}
				if(contentstarted) Serial.print(c);
				if(c=='[') contentstarted = true;
				
			}
		}
		Serial.println();
		Serial.println("disconnecting.");
		client.stop();

		// precip[0] ?:00
		// precip[1] ?:05
		// 
		// precip[11] ?:55

	} 
	else {
		// kf you didn't get a connection to the server:
		Serial.println("connection failed");
	}

	
}

void loop()
{
	
}