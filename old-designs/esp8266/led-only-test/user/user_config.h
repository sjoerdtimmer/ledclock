
#ifndef USERCONFIGH
#define USERCONFIGH

#include <ip_addr.h>
#include <c_types.h>
#include <espconn.h>
// #include <espconn_secure.h>
#include "string.h"
#include "stdio.h"
#include "user_interface.h"
#include "mem.h"
#include "osapi.h"
#include "ets_sys.h"
#include "gpio.h"

#include "driver/uart.h"


// #define SSID "timmer"
// #define PASSWD "<HIDDEN>"
#define SSID "randomdata"
#define PASSWD "<HIDDEN>"

#define GETSTR "GET /~3118479/waterlevel/add/%3d HTTP/1.1\nHost: www.staff.science.uu.nl\n\n"
#define SAMPLES 5

#define SECONDS *1000000
#define MINUTES SECONDS*60

#define SLEEP_TIME 10 SECONDS

#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];

static volatile os_timer_t dhcp_timer;

static struct espconn socket;
static esp_tcp tcpconfig;



static void ICACHE_FLASH_ATTR connectToServer();
static void ICACHE_FLASH_ATTR loop(os_event_t *events);
static void ICACHE_FLASH_ATTR gpio_start();
static void ICACHE_FLASH_ATTR gpio_finish();
static uint8_t ICACHE_FLASH_ATTR measure();
static void ICACHE_FLASH_ATTR init_wifi();
static void ICACHE_FLASH_ATTR transferComplete(void *arg);
static void ICACHE_FLASH_ATTR sendTheData(void *arg);
static void ICACHE_FLASH_ATTR onDisconnect(void *arg);
static void ICACHE_FLASH_ATTR onError(void *arg,err_t err);
static void ICACHE_FLASH_ATTR connectToServer();
static void ICACHE_FLASH_ATTR wait_for_wifi();
void ICACHE_FLASH_ATTR user_init();


#endif
