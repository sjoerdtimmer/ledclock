/**
 * Hue. 
 * 
 * Hue is the color reflected from or transmitted through an object 
 * and is typically referred to as the name of the color (red, blue, yellow, etc.) 
 * Move the cursor vertically over each bar to alter its hue. 
 */
 
int clock_r = 120;
int led_r = 5;

int numleds = 72;

int speed1 = 4;
int speed2 = 1;

boolean[] leds;

void setup() {
  size(640, 360);
  // colorMode(HSB, height, height, height);  
  background(0);
  smooth(4);
  frameRate(25);
  // frameRate(3);
  leds = new boolean[numleds];
  for(int i=0;i<numleds;i++){
    leds[i] = false;
  }
}

int t1=18,t2=19;


void draw() {
  size(width,height);
  
  for(int i=0;i<speed1;i++){
    leds[t1] = !leds[t1];
    t1 = (t1+numleds-1) % numleds;
  }
  for(int i=0;i<speed2;i++){
    leds[t2] = !leds[t2];
    t2 = (t2+numleds+1) % numleds;
  }
  
  
  background(0);
  
  for(int i=0;i<numleds;i++){
    float angle = PI*2*i/numleds;
    if(leds[i])fill(200,0,0);
    else fill(40);
    ellipse(width/2+clock_r*cos(angle),height/2-clock_r*sin(angle),led_r,led_r);
  }
}
