#ifndef WEATHER_H
#define WEATHER_H

#include "c_types.h"
#include "user_interface.h"
#include "user_config.h"
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "espconn.h"
#include "httpclient.h"

#define WEATHER_TIMEOUT_MS 5000
#define WEATHER_INTERVAL_MS 300000 // 5 minutes

#define JSONURL "http://buienalarm.nl/app/forecast.php?type=json&x=366&y=429"
// example response: {"start":1445629800,"temp":10,"precip":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"levels":{"light":0.25,"moderate":1,"heavy":2.5}} 

void ICACHE_FLASH_ATTR on_response(char * response, int http_status, char * full_response);
void ICACHE_FLASH_ATTR on_timeout();
void ICACHE_FLASH_ATTR weather_request_update();
void ICACHE_FLASH_ATTR weather_init();

#endif