#include "weather.h"

os_timer_t weather_timer;
uint8_t weather_timeouts = 0;

uint8_t weatherdata[25];


// example response: {"start":1445629800,"temp":10,"precip":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],"levels":{"light":0.25,"moderate":1,"heavy":2.5}} 
void ICACHE_FLASH_ATTR parse_data(char * data) {
	int i=0;
	int len = strlen(data);
	//  skip until the first '['
	while(i<len && data[i]!='[') i++;
	// i now points to the '['
	i++;
	// os_printf("first digit found at position %d\n\r",i);
	// i now point to the first digit
	uint8_t d;
	for(d=0;d<25;d++){
		weatherdata[d] = 0;
		while(data[i]!=',' && data[i]!=']'){
			weatherdata[d] *= 10;
			weatherdata[d] += data[i] - '0';
			i++;
		}
		i++; // skip the ','
		// os_printf("next digit at %d\n\r",i);
	}
	os_printf("weather data arrar: [");
	for(d=0;d<25;d++){
		os_printf("%d,",weatherdata[d]);
	}
	os_printf("]\n\r");
}


void ICACHE_FLASH_ATTR on_response(char * response, int http_status, char * full_response){
	weather_timeouts = 0;
	os_printf("http_status=%d\n", http_status);
	if (http_status != HTTP_STATUS_GENERIC_ERROR) {
		os_printf("strlen(full_response)=%d\n", strlen(full_response));
		os_printf("response=%s<EOF>\n", response);
	}

	// parse json:
	// very unportable!
	parse_data(response);

	// schedule next update:
	weather_init();
}




void ICACHE_FLASH_ATTR on_timeout(){
	weather_timeouts++;
	if(weather_timeouts > 3){
		system_restart();
	}
	weather_request_update();
}


void ICACHE_FLASH_ATTR weather_request_update(){
	os_timer_disarm(&weather_timer);
	os_timer_setfn(&weather_timer, (os_timer_func_t*)on_timeout, NULL);
	os_timer_arm(&weather_timer, WEATHER_TIMEOUT_MS, 0);


	http_raw_request(
		"buienalarm.nl",
		80,
		false,
		"/app/forecast.php?type=json&x=366&y=429",
		"",
		"", (http_callback)on_response);
}


void ICACHE_FLASH_ATTR weather_init(){
	os_timer_disarm(&weather_timer);
	os_timer_setfn(&weather_timer,(os_timer_func_t*)weather_request_update,NULL);
	os_timer_arm(&weather_timer,WEATHER_INTERVAL_MS,0);
}