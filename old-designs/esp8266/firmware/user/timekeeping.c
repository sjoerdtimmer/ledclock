#include "timekeeping.h"


uint8 ntp_server[] = {131, 107, 13, 100}; // microsoft



//////////////////////////////////////////////////////////////////////
//        some function that keep track of time since system bootup //
//////////////////////////////////////////////////////////////////////
uint32_t millis;
uint32_t prevUS;
// this works, even if system_get_time() overflows because the (-) operation will underflow
uint32_t ICACHE_FLASH_ATTR time_get_millis_since_boot() {
  while (system_get_time() - prevUS >= 1000){
    millis++;
    prevUS += 1000; 
  }
  return millis;
}
// handy function to make all wait animation synchonised
int ICACHE_FLASH_ATTR time_get_animation_frame() {
	return time_get_millis_since_boot() / 50 % 72; // about 1.5 times round (72 leds) every seconds
}



/////////////////////////////////////////////////////////////////////
//  keeping track of real time, millis are not synchonised         //
/////////////////////////////////////////////////////////////////////
uint32_t unixtime;
uint32_t prevmillis;
void ICACHE_FLASH_ATTR time_set_unix(uint32_t s) {
	unixtime = s;
	prevmillis = time_get_millis_since_boot();
}

uint32_t ICACHE_FLASH_ATTR time_get_unix() {
	while(time_get_millis_since_boot() - prevmillis >= 1000){
		unixtime++;
		prevmillis += 1000;
	}
	return unixtime;
}

uint32_t ICACHE_FLASH_ATTR time_get_hours() {
	return ((time_get_unix() / 3600) + TIMEZONE) % 24;
}

uint32_t ICACHE_FLASH_ATTR time_get_minutes() {
	return (time_get_unix() / 60) % 60;
}

uint32_t ICACHE_FLASH_ATTR time_get_seconds() {
	return time_get_unix() % 60;
}

// millis into the current second
uint32_t ICACHE_FLASH_ATTR time_get_millis() {
	time_get_unix(); // make sure that we are at most 999 ms behind schedule
	return time_get_millis_since_boot() - prevmillis;
}






////////////////////////////////////////////////////
// code to synchonise time over network via sntp  //
////////////////////////////////////////////////////




void ICACHE_FLASH_ATTR ntp_timeout_callback(void *arg) {
	
	os_timer_disarm(&timeout_timer);
	#ifndef SUPPRESS_ALL_SERIAL
	os_printf("ntp timed out\n\r");
	#endif

	if(ntp_timeouts++>FAILLEVEL_TIMEOUTS){
		system_restart();
	}


	// clean up connection
	if (pCon) {
		espconn_delete(pCon);
		os_free(pCon->proto.udp);
		os_free(pCon);
		pCon= 0;
	}

	// initiate another attempt:
	os_timer_setfn(&timeout_timer,(os_timer_func_t *)ntp_wait_for_wifi,NULL);
    os_timer_arm(&timeout_timer, 1000, 0); 
}



void ICACHE_FLASH_ATTR ntp_udp_recv(void *arg, char *pdata, unsigned short len) {
	
	struct tm *dt;
	time_t timestamp;
	ntp_t *ntp;

	os_timer_disarm(&timeout_timer);



	ntp_timeouts = 0;

	// extract ntp time
	ntp = (ntp_t*)pdata;
	timestamp = ntp->trans_time[0] << 24 | ntp->trans_time[1] << 16 |ntp->trans_time[2] << 8 | ntp->trans_time[3];

	// convert to unix time:
	timestamp -= 2208988800UL;

	#ifndef SUPPRESS_ALL_SERIAL
	os_printf("got NTP time %d\n\r",timestamp);
	#endif

	// set the system time:
	time_set_unix(timestamp);

	// clean up connection
	if (pCon) {
		espconn_delete(pCon);
		os_free(pCon->proto.udp);
		os_free(pCon);
		pCon = 0;
	}

	// schedule the next update:
	os_timer_setfn(&network_timer,(os_timer_func_t *)ntp_wait_for_wifi,NULL);  // when the time elapsed, first check that wifi is still up
    os_timer_arm(&network_timer, NTP_INTERVAL_MS, 0); // let's do it again sometime
}






// this function sends out the request
// and schedules a timeout 
void ICACHE_FLASH_ATTR ntp_request_update() {
	ntp_t ntp;

	// set up the udp "connection"
	pCon = (struct espconn*)os_zalloc(sizeof(struct espconn));
	pCon->type = ESPCONN_UDP;
	pCon->state = ESPCONN_NONE;
	pCon->proto.udp = (esp_udp*)os_zalloc(sizeof(esp_udp));
	pCon->proto.udp->local_port = espconn_port();
	pCon->proto.udp->remote_port = 123;
	os_memcpy(pCon->proto.udp->remote_ip, ntp_server, 4);

	// create a really simple ntp request packet
	os_memset(&ntp, 0, sizeof(ntp_t));
	ntp.options = 0b00100011; // leap = 0, version = 4, mode = 3 (client)

	// set timeout timer
	os_timer_disarm(&timeout_timer);
	os_timer_setfn(&timeout_timer, (os_timer_func_t*)ntp_timeout_callback, pCon);
	os_timer_arm(&timeout_timer, NTP_TIMEOUT_MS, 0);

	// send the ntp request
	espconn_create(pCon);
	espconn_regist_recvcb(pCon, ntp_udp_recv);
	espconn_sent(pCon, (uint8*)&ntp, sizeof(ntp_t));
}



// this is the first function to call
// it checks if wifi is up, and starts the ntp request if it is
// if not, it reschedules itself for some later time
void ICACHE_FLASH_ATTR ntp_wait_for_wifi(){
    os_timer_disarm(&network_timer);
    struct ip_info ipConfig;
    wifi_get_ip_info(STATION_IF,&ipConfig);
    if(wifi_station_get_connect_status() == STATION_GOT_IP && ipConfig.ip.addr !=0){
        // we are connected!
        #ifndef SUPPRESS_ALL_SERIAL
        os_printf("wifi is up!\n\r");
        #endif
        wifi_timeouts = 0;
        ntp_request_update();
    }else {
    	if(wifi_timeouts++ > FAILLEVEL_TIMEOUTS){
    		// system_restart();
    		#ifndef SUPPRESS_ALL_SERIAL
    		os_printf("maximum timeout reached!\n\r");
    		#endif
    	}
    	#ifndef SUPPRESS_ALL_SERIAL
    	os_printf("still waiting for wifi... (%d)\n\r",wifi_timeouts);
    	#endif
    	os_timer_setfn(&network_timer,(os_timer_func_t *)ntp_wait_for_wifi,NULL);
    	os_timer_arm(&network_timer,1000,0);
    }
}

