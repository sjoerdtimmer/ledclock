#ifndef __TIMEKEEPING_H__
#define __TIMEKEEPING_H__
#include "mem.h"
#include "c_types.h"
#include "user_interface.h"
#include "user_config.h"
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "espconn.h"
#include <time.h>

#define NTP_TIMEOUT_MS 5000
#define NTP_INTERVAL_MS 100000

#define TIMEZONE 1

typedef struct {
	uint8 options;
	uint8 stratum;
	uint8 poll;
	uint8 precision;
	uint32 root_delay;
	uint32 root_disp;
	uint32 ref_id;
	uint8 ref_time[8];
	uint8 orig_time[8];
	uint8 recv_time[8];
	uint8 trans_time[8];
} ntp_t;



// the below are shared between wifi connection and tcp connection timeouts
// stop showing the time after WARNLEVEL failed updates
#define WARNLEVEL_TIMEOUTS 3
// reboot module after FAILLEVEL failed updates
#define FAILLEVEL_TIMEOUTS 10

// list of major public servers http://tf.nist.gov/tf-cgi/servers.cgi


static os_timer_t network_timer;
static os_timer_t timeout_timer;
static struct espconn *pCon = 0;


uint8_t wifi_timeouts;
uint8_t ntp_timeouts;


uint32_t ICACHE_FLASH_ATTR time_get_millis_since_boot();
int ICACHE_FLASH_ATTR time_get_animation_frame();
uint32_t ICACHE_FLASH_ATTR time_get_unix();


void ICACHE_FLASH_ATTR time_set_unix(uint32_t s);

uint32_t ICACHE_FLASH_ATTR time_get_hours();
uint32_t ICACHE_FLASH_ATTR time_get_minutes();
uint32_t ICACHE_FLASH_ATTR time_get_seconds();
uint32_t ICACHE_FLASH_ATTR time_get_millis();


void ICACHE_FLASH_ATTR ntp_timeout(void *arg);
void ICACHE_FLASH_ATTR ntp_udp_recv(void *arg, char *pdata, unsigned short len);
void ICACHE_FLASH_ATTR ntp_request_update();
void ICACHE_FLASH_ATTR ntp_wait_for_wifi();
int ICACHE_FLASH_ATTR  ntp_get_status();

#endif