
#include "webserver.h"

struct espconn serverconn;

// const char *msg_welcome = "Welcome!\n\n";


void ICACHE_FLASH_ATTR www_disconcb(void *arg) {
    struct espconn *pespconn = (struct espconn *) arg;
    // os_printf("tcp connection disconnected\n");
}

void ICACHE_FLASH_ATTR www_sentcb(void *arg) {
    struct espconn *pespconn = (struct espconn *) arg;
    // os_printf("data sent, disconnecting now\n");
    // the manual explicitly forbids the use of disconnect in a callback,
    // but hey, it works >;-)
    espconn_disconnect(pespconn);
}



void ICACHE_FLASH_ATTR www_recvcb(void *arg, char *pusrdata, unsigned short length)
{
    struct espconn *pespconn = (struct espconn *) arg;

    // get the GET string:
    int getstart = 0;
    while(pusrdata[getstart] != '/'){getstart++;}
    int getstop = getstart;
    while(pusrdata[getstop] != ' '){getstop++;}
    char getstring[getstop-getstart+1];
	int i;
	for(i=getstart;i<getstop;i++){
		getstring[i-getstart] = pusrdata[i];
	}
	getstring[getstop-getstart] = 0;
	os_printf("GET request for \"%s\"\n\r",getstring);

	if(strcmp(getstring,"/post")==0) {
		os_printf("raw tcp request: '%s' ", pusrdata);
	} else {// get the main page 
		espconn_sent(pespconn, (uint8*)minified_html, minified_html_len);
	}


    // if(length >= 2 && pusrdata[0] == 0xFF && (pusrdata[1] & 0xF0) == 0xF0) {        
        // espconn_sent(pespconn, (uint8*)msg_welcome, os_strlen(msg_welcome));
    
    // } else {
        // os_printf("incomming tcp request: '%s' ", pusrdata);

        // int i;
        // for(i = 0; i < length; i++) {
        //     os_printf("0x%02X ", pusrdata[i]);
        // }
        // os_printf("\n");
        // espconn_sent(pespconn, pusrdata, length); //echo
    // }
}

void ICACHE_FLASH_ATTR webserver_connectcb(void *arg)
{
    struct espconn *pespconn = (struct espconn *)arg;

    // os_printf("tcp connection established\n");

    espconn_regist_recvcb(pespconn, www_recvcb);
    // espconn_regist_reconcb(pespconn, tcpserver_recon_cb);
    espconn_regist_disconcb(pespconn, www_disconcb);
    espconn_regist_sentcb(pespconn, www_sentcb);
}

void ICACHE_FLASH_ATTR webserver_init(void)
{
    serverconn.type = ESPCONN_TCP;
    serverconn.state = ESPCONN_NONE;
    serverconn.proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
    serverconn.proto.tcp->local_port = 80;
    espconn_regist_connectcb(&serverconn, webserver_connectcb);
    espconn_accept(&serverconn);
    espconn_regist_time(&serverconn, 12*3600, 0);

    // os_printf("[%s] initializing shell!\n", __func__);
}
