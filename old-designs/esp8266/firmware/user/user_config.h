
#ifndef USERCONFIGH
#define USERCONFIGH


#include "driver/uart.h"
#include <ip_addr.h>
#include <c_types.h>
#include <espconn.h>
#include "string.h"
#include "stdio.h"
#include "user_interface.h"
#include "mem.h"
#include "osapi.h"
// #include "smart.h"
#include "smartconfig.h"
#include "timekeeping.h"
#include "displayfunctions.h"
#include "ws2812.h"
#include "weather.h"

// necessary for standalone operation:
#define SUPPRESS_ALL_SERIAL

#define APSSID "ledclock"
#define APPASSWD "ledclock"


#define SSID "timmer"
#define PASSWD "<HIDDEN>"
// #define SSID "randomdata"
// #define PASSWD "<HIDDEN>"


#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];

static volatile os_timer_t display_timer;

void ICACHE_FLASH_ATTR display_single_red_led();
void ICACHE_FLASH_ATTR display_red_spinner();
void ICACHE_FLASH_ATTR display_time();
void ICACHE_FLASH_ATTR displayloop();
void ICACHE_FLASH_ATTR init_wifi();
void ICACHE_FLASH_ATTR user_init(void);
void ICACHE_FLASH_ATTR loop(os_event_t *events);



#endif
