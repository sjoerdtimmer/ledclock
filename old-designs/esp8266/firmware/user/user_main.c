
#include "user_config.h"


/* architecture note: 
   the display is updated regularly in the displayloop
   This design (above individual functions updating the display, possibly with busy waits)
   prevents any kind of race conditions.
*/





void ICACHE_FLASH_ATTR output_leds() {
    char outbuffer[3*NUMLEDS];
    uint8_t i;
    for(i=0;i<NUMLEDS;i++){
        outbuffer[3*i+0] = green[i];
        outbuffer[3*i+1] = red[i];
        outbuffer[3*i+2] = blue[i];
    }
    // ets_wdt_disable();

    // WS2812OutBuffer( outbuffer, sizeof(outbuffer) );
    // ws2812_push(outbuffer,sizeof(outbuffer));
    WS2812OutBuffer( outbuffer, sizeof(outbuffer) );
}



#define LVL0 5
#define LVL1 127
#define LVL2 200
void ICACHE_FLASH_ATTR output_text() {
    char outbuffer[NUMLEDS+1];
    outbuffer[NUMLEDS] = 0;

    uint8_t i;
    for(i=0;i<NUMLEDS;i++){
        if(red[i]<LVL0){
            outbuffer[i] = ' ';
        }else if(red[i]<LVL1){
            outbuffer[i] = '-';
        }else if(red[i]<LVL2){
            outbuffer[i] = '+';
        }else{
            outbuffer[i] = '#';
        }
    }
    os_printf("R: %s\n\r",outbuffer);
    for(i=0;i<NUMLEDS;i++){
        if(green[i]<LVL0){
            outbuffer[i] = ' ';
        }else if(green[i]<LVL1){
            outbuffer[i] = '-';
        }else if(green[i]<LVL2){
            outbuffer[i] = '+';
        }else{
            outbuffer[i] = '#';
        }
    }
    os_printf("G: %s\n\r",outbuffer);
    for(i=0;i<NUMLEDS;i++){
        if(blue[i]<LVL0){
            outbuffer[i] = ' ';
        }else if(blue[i]<LVL1){
            outbuffer[i] = '-';
        }else if(blue[i]<LVL2){
            outbuffer[i] = '+';
        }else{
            outbuffer[i] = '#';
        }
    }
    os_printf("B: %s\n\r",outbuffer);
}


void ICACHE_FLASH_ATTR displayloop() {
    os_timer_disarm(&display_timer);
    // os_printf("%d\n\r",time_get_unix());
    // os_printf("it is %02d:%02d:%02d+%04d\n\r",time_get_hours(),time_get_minutes(),time_get_seconds(),time_get_millis());

    // os_printf("timeout status:\t%d\t%d\n\r",wifi_timeouts,ntp_timeouts);
    if(wifi_timeouts >= WARNLEVEL_TIMEOUTS){
        #ifndef SUPPRESS_ALL_SERIAL
        os_printf("dhcp progress\n\r");
        #endif
        display_red_spinner();
    }else if(ntp_timeouts >= WARNLEVEL_TIMEOUTS){
        #ifndef SUPPRESS_ALL_SERIAL
        os_printf("ntp progress\n\r");
        #endif
        display_red_spinner();
    // }else if() {
    }else{
        display_time_v4();
    }

    #ifndef SUPPRESS_ALL_SERIAL
    // os_printf("it is %02d:%02d:%02d+%04d\n\r",time_get_hours(),time_get_minutes(),time_get_seconds(),time_get_millis());
    output_text();
    #endif
    output_leds();
        
    os_timer_setfn(&display_timer,(os_timer_func_t *)displayloop,NULL);
    os_timer_arm(&display_timer,50,0); // update in 100ms (10 fps)
}








void ICACHE_FLASH_ATTR init_wifi() {
    char ssid[32] = SSID;
    char pass[32] = PASSWD;
    char apssid[32] = APSSID;
    char appass[32] = APPASSWD;

    espconn_tcp_set_max_con(5);

    if (wifi_get_opmode() != STATIONAP_MODE) {
        ETS_UART_INTR_DISABLE();
        wifi_set_opmode(  STATIONAP_MODE ); //STATION_MODE
        ETS_UART_INTR_ENABLE();
    }

    struct softap_config apConf;
    // Configure the AP
    strcpy(apConf.ssid, APSSID);
    strcpy(apConf.password, APPASSWD);
    apConf.authmode = AUTH_WPA2_PSK;
    

    struct station_config stationConf;
    // Configure the AP
    strcpy(stationConf.ssid, ssid);
    strcpy(stationConf.password, pass);
    stationConf.bssid_set = 0;    // very important or the scan will not be executed!

    ETS_UART_INTR_DISABLE();
    wifi_softap_set_config(&apConf);
    wifi_station_set_config(&stationConf);
    wifi_station_connect();
    ETS_UART_INTR_ENABLE();
    wifi_softap_dhcps_start();
    wifi_station_dhcpc_start();
}




// void ICACHE_FLASH_ATTR
// smartconfig_done(sc_status status, void *pdata)
// {
//     switch(status) {
//         case SC_STATUS_WAIT:
//             #ifndef SUPPRESS_ALL_SERIAL
//             os_printf("SC_STATUS_WAIT\n");
//             #endif
//             break;
//         case SC_STATUS_FIND_CHANNEL:
//             #ifndef SUPPRESS_ALL_SERIAL
//             os_printf("SC_STATUS_FIND_CHANNEL\n");
//             #endif
//             break;
//         case SC_STATUS_GETTING_SSID_PSWD:
//             #ifndef SUPPRESS_ALL_SERIAL
//             os_printf("SC_STATUS_GETTING_SSID_PSWD\n");
//             #endif
//             sc_type *type = pdata;
//             if (*type == SC_TYPE_ESPTOUCH) {
//                 #ifndef SUPPRESS_ALL_SERIAL
//                 os_printf("SC_TYPE:SC_TYPE_ESPTOUCH\n");
//                 #endif
//             } else {
//                 #ifndef SUPPRESS_ALL_SERIAL
//                 os_printf("SC_TYPE:SC_TYPE_AIRKISS\n");
//                 #endif
//             }
//             break;
//         case SC_STATUS_LINK:
//             #ifndef SUPPRESS_ALL_SERIAL
//             os_printf("SC_STATUS_LINK\n");
//             #endif
//             struct station_config *sta_conf = pdata;
    
//             wifi_station_set_config(sta_conf);
//             wifi_station_disconnect();
//             wifi_station_connect();
//             break;
//         case SC_STATUS_LINK_OVER:
//             #ifndef SUPPRESS_ALL_SERIAL
//             os_printf("SC_STATUS_LINK_OVER\n");
//             #endif
//             if (pdata != NULL) {
//                 uint8 phone_ip[4] = {0};

//                 os_memcpy(phone_ip, (uint8*)pdata, 4);
//                 #ifndef SUPPRESS_ALL_SERIAL
//                 os_printf("Phone ip: %d.%d.%d.%d\n",phone_ip[0],phone_ip[1],phone_ip[2],phone_ip[3]);
//                 #endif
//             }
//             smartconfig_stop();
//             break;
//     }
    
// }



void ICACHE_FLASH_ATTR user_init(void) {
    #ifndef SUPPRESS_ALL_SERIAL
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
    #endif

    // we bootup the module with 3 timeouts so that it immediately shows the spinners
    wifi_timeouts = WARNLEVEL_TIMEOUTS;
    ntp_timeouts  = WARNLEVEL_TIMEOUTS;

	
    wifi_set_opmode(STATION_MODE);
    // smartconfig_start(smartconfig_done);
    // this replaces my own:
    init_wifi();

    webserver_init();

    // now wait until we are connected:
	ntp_wait_for_wifi();

    displayloop();

    // start a background worker that will first wait 5 minutes and then start weather updates every 5 minutes
    weather_init();
	
	//Add at least one os thread to keep the watchdog at bay:
	system_os_task(loop, user_procTaskPrio, user_procTaskQueue, user_procTaskQueueLen);
	system_os_post(user_procTaskPrio, 0, 0 );

    #ifndef SUPPRESS_ALL_SERIAL
    os_printf("init done\n\r");
    #endif
}


/* this loop is mandatory, even when not used because the watchdog timer will reboot the chip
   if no system_os_tasks are executed for too long... */
void ICACHE_FLASH_ATTR loop(os_event_t *events) {

    os_delay_us(100);
    system_os_post(user_procTaskPrio, 0, 0);
}


// TODO: add smartconfig or the like: this is an example:
// https://github.com/CHERTS/esp8266-devkit/blob/master/Espressif/examples/smart_config/user/user_main.c

// other interesting read about the esp8266 and bitbanging
// http://naberius.de/2015/05/14/esp8266-gpio-output-performance/

/* NOTES:
states and visuals:
initially:
	* off					off
	* getting ip			rotating dot
	* getting time			rotating growing circle
	* running				show time

exceptional: 
	if it fails to get the time 3 times in a row it shows the spinner
    if it fails to get the time 3 more times it reboots
*/