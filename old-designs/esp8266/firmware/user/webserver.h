#ifndef WEBSERVER_H
#define WEBSERVER_H
#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "lwip/stats.h"

#include "espconn.h"

#include "html.h"

void ICACHE_FLASH_ATTR
webserver_init(void);

#endif