#ifndef DISPLAYFUNCTIONSH
#define DISPLAYFUNCTIONSH

#include <c_types.h>
#include <stdbool.h>
#include <math.h>
#include "user_interface.h"

#define NUMLEDS 72
uint8_t red[NUMLEDS];
uint8_t green[NUMLEDS];
uint8_t blue[NUMLEDS];


enum color_enum {
    COLOR_RED,
    COLOR_GREEN,
    COLOR_BLUE
};
typedef enum color_enum color_t; 

// it is easy to have a transformation function so that physical rotation or mirroring of the clock
// can easily be compensated. the below rotates 45' (9 out of 72) and reverses on a 72 led strip. 
// #define ROTATE(x) ((-(x)+9+NUMLEDS)%NUMLEDS)
// for debugging it is easier to have no rotation at all
#define ROTATE(x) (((-x)+NUMLEDS)%NUMLEDS)



void ICACHE_FLASH_ATTR display_red_spinner();
void ICACHE_FLASH_ATTR display_time();
void ICACHE_FLASH_ATTR display_time_v2();
void ICACHE_FLASH_ATTR display_time_v3();
void ICACHE_FLASH_ATTR display_time_v4();
void ICACHE_FLASH_ATTR showArc(double start, double stop, color_t color);

#endif