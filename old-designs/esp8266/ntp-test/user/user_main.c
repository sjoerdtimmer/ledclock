// #include "mem.h"
// #include "c_types.h"
// #include "user_interface.h"
// #include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
// #include "espconn.h"
#include "user_config.h"


/* main loop: */
static void ICACHE_FLASH_ATTR loop(os_event_t *events) {
	os_delay_us(100);
	system_os_post(user_procTaskPrio, 0, 0);
}



void ICACHE_FLASH_ATTR mymainloop(){
	os_timer_disarm(&loop_timer);

	ntp_get_time();

	os_timer_setfn(&loop_timer,(os_timer_func_t *)mymainloop,NULL);
    os_timer_arm(&loop_timer,5000,0);
}


void ICACHE_FLASH_ATTR init_wifi(){
    char ssid[32] = SSID;
    char pass[32] = PASSWD;

    espconn_tcp_set_max_con(5);

    if (wifi_get_opmode() != STATION_MODE) {
        ETS_UART_INTR_DISABLE();
        wifi_set_opmode(  STATION_MODE ); //STATION_MODE
        ETS_UART_INTR_ENABLE();
    }

    struct station_config wifiConf;
    // Configure the AP
    strcpy(wifiConf.ssid, ssid);
    strcpy(wifiConf.password, pass);
    wifiConf.bssid_set = 0;    // very important or the scan will not be executed!

    ETS_UART_INTR_DISABLE();
    wifi_station_set_config(&wifiConf);
    wifi_station_connect();
    ETS_UART_INTR_ENABLE();
    wifi_station_dhcpc_start();
}


void ICACHE_FLASH_ATTR wait_for_wifi(){
    os_timer_disarm(&loop_timer);
    struct ip_info ipConfig;
    wifi_get_ip_info(STATION_IF,&ipConfig);
    if(wifi_station_get_connect_status() == STATION_GOT_IP && ipConfig.ip.addr !=0){
    	// we are connected!
        os_printf("wifi now up!\n\r");
        mymainloop();
    }else {
        os_printf("still waiting for dhcp...\n\r");
        os_timer_setfn(&loop_timer,(os_timer_func_t *)wait_for_wifi,NULL);
        os_timer_arm(&loop_timer,1000,0);
    }
    
}





void ICACHE_FLASH_ATTR user_init(void)
{
	uart_init(BIT_RATE_9600, BIT_RATE_9600);

	init_wifi();
	wait_for_wifi();
	
	//Add at least one os thread to keep the watchdog at bay:
	system_os_task(loop, user_procTaskPrio, user_procTaskQueue, user_procTaskQueueLen);
	system_os_post(user_procTaskPrio, 0, 0 );
}






/* NOTES:
states and visuals:
initially:
	* off					off
	* getting ip			rotating dot
	* getting time			rotating growing circle
	* running				show time

exceptional: running but...
	* connection lost		blink the minute dot
	* ntp error				blink the hour dot
	* weather error			blink the hour dot

things to display on local wifi:
	* time
	* wifi configuration: both ap and sta ssid and passwd
	* last ntp response and time
	* last weather response and time

architecture:
	* ws2812.c       handles the display
	* webinterface.c handles configuration page
	* time.c         handles timekeeping
	* ntp.c          handles ntp requests
	* weather.c      handles precipitation forecasts
	* user_main.c    ties things together.
*/