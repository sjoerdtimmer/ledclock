#include "mem.h"
#include "c_types.h"
#include "user_interface.h"
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "espconn.h"




/* main loop: */
void ICACHE_FLASH_ATTR
loop(os_event_t *events) {
	os_delay_us(100);
	system_os_post(user_procTaskPrio, 0, 0);
}



void user_init(void)
{
	uart_init(BIT_RATE_115200, BIT_RATE_115200);

	ws2812_begin(6);
	// TODO figure out how to safely re-allow this.
	ets_wdt_disable();

	//Add at least one os thread to keep the watchdog at bay:
	system_os_task(loop, user_procTaskPrio, user_procTaskQueue, user_procTaskQueueLen);
	system_os_post(user_procTaskPrio, 0, 0 );
}






/* NOTES:
states and visuals:
initially:
	* off					off
	* getting ip			rotating dot
	* getting time			rotating growing circle
	* running				show time

exceptional: running but...
	* connection lost		blink the minute dot
	* ntp error				blink the hour dot
	* weather error			blink the hour dot

things to display on local wifi:
	* time
	* wifi configuration: both ap and sta ssid and passwd
	* last ntp response and time
	* last weather response and time

architecture:
	* ws2812.c       handles the display
	* webinterface.c handles configuration page
	* time.c         handles timekeeping
	* ntp.c          handles ntp requests
	* weather.c      handles precipitation forecasts
	* user_main.c    ties things together.
*/