#ifndef __WS2812_H__
#define __WS2812_H__

#define WSGPIO 0
#define INVERTED

#include "c_types.h"
#include "user_interface.h"
#include "ets_sys.h"
#include "gpio.h"

//You will have to 	os_intr_lock();  	os_intr_unlock();

// void ws2812_write_raw( uint8_t * buffer, uint16_t length );

void ws2812_begin(int numleds);

void ws2812_set_led(int led, uint8_t r, uint8_t g, uint8_t b);

void ws2812_update();

void ws2812_clear();


#endif

