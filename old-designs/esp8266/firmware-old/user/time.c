#include "time.h"

static volatile os_timer_t time_timer;

uint32_t unixtime;

void time_step(void *arg) {
	unixtime++;
}

void time_begin() {
	os_timer_disarm(&time_timer);
	os_timer_setfn(&time_timer, (os_timer_func_t *)time_step, NULL);

	os_timer_arm(&time_timer, 1000, 1); // 1 indicates repeating timer, every 1000 ms
}

void time_set(uint32_t t) {
	unixtime = t;
}

uint32_t time_get() {
	return unixtime;
}