
#ifndef USERCONFIGH
#define USERCONFIGH

#include <ip_addr.h>
#include <c_types.h>
#include <espconn.h>
#include "string.h"
#include "stdio.h"
#include "user_interface.h"
#include "mem.h"
#include "osapi.h"
#include "ets_sys.h"
#include "gpio.h"
#include "driver/uart.h"
#include "ws2812.h"

#define printf( ... ) os_sprintf( generic_print_buffer, __VA_ARGS__ );  uart0_sendStr( generic_print_buffer );


// #define SSID "timmer"
// #define PASSWD "<HIDDEN>"
#define SSID "randomdata"
#define PASSWD "<HIDDEN>"


#define user_procTaskPrio        0
#define user_procTaskQueueLen    1
os_event_t    user_procTaskQueue[user_procTaskQueueLen];



void ICACHE_FLASH_ATTR loop(os_event_t *events);
void ICACHE_FLASH_ATTR user_init();


#endif