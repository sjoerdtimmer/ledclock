#include "ws2812.h"
#include "ets_sys.h"
#include "osapi.h"


#define GPIO_OUTPUT_SET(gpio_no, bit_value) \
	gpio_output_set(bit_value<<gpio_no, ((~bit_value)&0x01)<<gpio_no, 1<<gpio_no,0)


//I just used a scope to figure out the right time periods.

void ws2812_send_0()
{
	uint8_t time = 8;
	#ifdef INVERTED
	WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 0 );
	WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 0 );
	#else
	WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 1 );
	WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 1 );
	#endif
	while(time--)
	{
		#ifdef INVERTED
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 1 );
		#else
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 0 );
		#endif
	}

}

void ws2812_send_1()
{
	uint8_t time = 9;
	while(time--)
	{
		#ifdef INVERTED
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 0 );
		#else
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 1 );
		#endif
	}
	time = 3;
	while(time--)
	{
		#ifdef INVERTED
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 1 );
		#else
		WRITE_PERI_REG( PERIPHS_GPIO_BASEADDR + GPIO_ID_PIN(WSGPIO), 0 );
		#endif
	}

}


void ws2812_write_raw( uint8_t * buffer, uint16_t length ) {
	uint16_t i;
	GPIO_OUTPUT_SET(GPIO_ID_PIN(WSGPIO), 0);
	for( i = 0; i < length; i++ )
	{
		uint8_t byte = buffer[i];
		if( byte & 0x80 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x40 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x20 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x10 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x08 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x04 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x02 ) ws2812_send_1(); else ws2812_send_0();
		if( byte & 0x01 ) ws2812_send_1(); else ws2812_send_0();
	}
	//reset will happen when it's low long enough.
	//(don't call this function twice within 10us)
}


typedef struct rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb;


rgb *ws2812_rgb_data;
int ws2812_numleds;

void ws2812_begin(int numleds) {
	ws2812_rgb_data = (rgb *)os_zalloc(numleds*sizeof(struct rgb));
	ws2812_numleds  = numleds;

	//Initialize the output.
	char outbuffer[] = { 0xff };
	ws2812_write_raw( outbuffer, 1 );
}

void ws2812_set_led(int led, uint8_t red, uint8_t green, uint8_t blue) {
	ws2812_rgb_data[led].r = red;
	ws2812_rgb_data[led].g = green;
	ws2812_rgb_data[led].b = blue;
}

void ws2812_update() {
	ws2812_write_raw((uint8_t *)ws2812_rgb_data, 3 * ws2812_numleds);
}

void ws2812_clear() {
	mem_free(ws2812_rgb_data);
	ws2812_rgb_data = (rgb *)os_zalloc(ws2812_numleds*sizeof(struct rgb));
}




