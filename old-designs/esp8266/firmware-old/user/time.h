#ifndef __TIME_H__
#define __TIME_H__
#include "mem.h"
#include "c_types.h"
#include "user_interface.h"
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "espconn.h"

void time_begin();
void time_set();
uint32_t time_get();

#endif