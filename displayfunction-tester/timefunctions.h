#ifndef TIMEFUNCTIONS_H
#define TIMEFUNCTIONS_H
#include <sys/time.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

void do_measure_time();

uint32_t time_get_hours();
uint32_t time_get_minutes();
uint32_t time_get_seconds();
uint32_t time_get_millis();

#endif