#include "timefunctions.h"
#include <stdio.h>
#include <sys/time.h>
#include <sys/timeb.h>


// #include <stdint.h>
struct timeval *tv;



void do_measure_time() {
	if(tv==NULL){
		printf("initializing tv\n");
		tv = (struct timeval *)calloc(1,sizeof (struct timeval));
	}
	gettimeofday(tv,NULL);
}


uint32_t time_get_hours() {
	do_measure_time();
	uint32_t res = (tv->tv_sec % 86400) / 3600;
	return res + 2;  // locale!!
}

uint32_t time_get_minutes() {
	do_measure_time();
	// struct timeval *tv;
	// tv = (struct timeval *)calloc(1,sizeof (struct timeval));
	// gettimeofday(tv,NULL);

	uint32_t res = (tv->tv_sec % 3600) / 60;
	// free(tv);
	return res;
}

uint32_t time_get_seconds() {
	do_measure_time();
	// struct timeval *tv;
	// tv = (struct timeval *)calloc(1,sizeof (struct timeval));
	// gettimeofday(tv,NULL);

	uint32_t res = tv->tv_sec % 60;
	// free(tv);
	return res;
}

uint32_t time_get_millis() {
	do_measure_time();
	// struct timeval *tv;
	// tv = (struct timeval *)calloc(1,sizeof (struct timeval));
	// gettimeofday(tv,NULL);

	uint32_t res = tv->tv_usec /1000;
	// free(tv);
	return res;
}