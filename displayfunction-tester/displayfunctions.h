#ifndef DISPLAYFUNCTION_H
#define DISPLAYFUNCTION_H
#include <stdio.h>
#include <stdint.h>
#include "timefunctions.h"


#define ICACHE_FLASH_ATTR 

#define NUMLEDS 72
uint8_t red[NUMLEDS];
uint8_t green[NUMLEDS];
uint8_t blue[NUMLEDS];

enum color_enum {
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BLUE
};

typedef enum color_enum color_t;


// typedef color_t enum color_enum;

#define ROTATE(x) (((x)+54)%NUMLEDS)


void ICACHE_FLASH_ATTR showArc(double start, double stop, color_t color);

void ICACHE_FLASH_ATTR display_time_v3();
void ICACHE_FLASH_ATTR display_time_v4();

#endif