// compile easily with:
// gcc -Wcast-align *.c -o main -lm `pkg-config --cflags --libs gtk+-3.0`

#include <stdio.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdint.h>
#include "displayfunctions.h"
#include "timefunctions.h"
#include <sys/time.h>
#include <math.h>

static void do_drawing(cairo_t *);

// #define clockradius 200
// #define ledradius 6

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {      
	// printf("%02d:%02d:%02d %04d\n\r",time_get_hours(),time_get_minutes(),time_get_seconds(),time_get_millis());
	// cairo_set_source_rgb(cr, 0, 0, 0);
	// cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
	// cairo_set_font_size(cr, 40.0);

	// cairo_move_to(cr, 10.0, 50.0);
	// cairo_show_text(cr, "Hello World!!");   
	int width  = gtk_widget_get_allocated_width (widget);
	int height = gtk_widget_get_allocated_height(widget);
	
	// int clockradius = MIN(width,height)/2-20;
	// int ledradius = clockradius/30;
	int clockradius = 3000;
	int ledradius = 100;
	// background fill:
	cairo_set_antialias(cr,CAIRO_ANTIALIAS_BEST);

	cairo_set_source_rgb(cr,0,0,0);
	cairo_rectangle(cr, 0, 0, width, height);
	cairo_fill(cr);

	// from here onwards use (0,0) as the center of the screen:
	cairo_translate(cr, width/2, height/2);
	cairo_scale(cr,0.0095*MIN(width,height)/60,0.0095*MIN(width,height)/60);

	for(int i=0;i<NUMLEDS;i++){
		cairo_set_source_rgb(cr,1.0/255*red[i],1.0/255*green[i],1.0/255*blue[i]);
		cairo_set_line_width(cr, 30);
		int cx = round(cos(M_PI*2*i/NUMLEDS)*clockradius);
		int cy = round(sin(M_PI*2*i/NUMLEDS)*clockradius);
		cairo_arc(cr, cx, cy, ledradius, 0, 2 * M_PI);
		// double ledangle = M_PI*2*i/NUMLEDS;
		// cairo_arc(cr,0,0,clockradius,ledangle-M_PI/NUMLEDS,ledangle+M_PI/NUMLEDS);
		// cairo_stroke(cr);
		cairo_fill(cr);
	}


	return FALSE; // don't draw anything on top?
}



// static void do_drawing(cairo_t *cr) {
  
//   
// }



gboolean timer_exe(gpointer * data){
	GtkWidget * area = (GtkWidget *) data;
	// printf("redrawing...\n\r");
	display_time_v4();
	gtk_widget_queue_draw(area);
	return TRUE; // do it again
}



int main(int argc, char *argv[]) {
	GtkWidget *window;
	GtkWidget *darea;

	// if (!g_thread_supported ()){ g_thread_init(NULL); }
	// gdk_threads_init();
	// gdk_threads_enter();

	gtk_init(&argc, &argv);

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	darea = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(window), darea);

	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL); 
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 400, 90); 
	gtk_window_set_title(GTK_WINDOW(window), "GTK window");

	gtk_widget_show_all(window);

	gdk_threads_add_timeout(50, (GSourceFunc)timer_exe, darea);

	gtk_main();

	// gdk_threads_leave();

	return 0;
}