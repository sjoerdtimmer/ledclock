#include "displayfunctions.h"
#include <sys/time.h>
#include <stdbool.h>
#include <math.h>

void ICACHE_FLASH_ATTR display_time_v3() {
    // fill seconds in red
    // I chose for all longs so that any calculation is done with longs as well because many of
    // the operations below here have large intermediate values:
    uint32_t millisIntoMinute = 1000UL * time_get_seconds() + time_get_millis();
    uint32_t numSecondPixels = millisIntoMinute * NUMLEDS / 60000;
    uint32_t millisIntoNextPixel = millisIntoMinute - numSecondPixels*60000/NUMLEDS; // remaining millis after the last whole pixel
    // % operator won't work here because of rounding of the (5000/6) part!

    // // same variables for minutes:
    uint32_t secondsIntoHour =  60UL * time_get_minutes() + time_get_seconds(); 
    uint32_t numMinPixels = secondsIntoHour * NUMLEDS / 3600;
    uint32_t secondsIntoNextPixel = secondsIntoHour - numMinPixels*3600/NUMLEDS;

    // same for hour pixels:
    uint32_t minutesIntoDay = 60UL * (time_get_hours()%12) + time_get_minutes();
    uint32_t numHourPixels = minutesIntoDay * NUMLEDS / 720;
    uint32_t minutesIntoNextPixel = minutesIntoDay - numHourPixels*720/NUMLEDS;

    // this variable indicates whether we are in the growing or shrinking 59 seconds cycle
    bool phase =  time_get_minutes()%2 == 0 && time_get_seconds() > time_get_minutes()
               || time_get_minutes()%2 == 1 && time_get_seconds() < time_get_minutes();
    
    uint8_t i;
    for (i=0;i < NUMLEDS ; i++) { 
        // printf("led %d: ",i);
        green[ROTATE(i)] = 0;
        if(time_get_seconds() > time_get_minutes() && i < numSecondPixels && i > numMinPixels){
            green[ROTATE(i)] = 255;
        }
        if(time_get_seconds() < time_get_minutes() && (i < numSecondPixels || i > numMinPixels)){
            green[ROTATE(i)] = 255;
        }
        if(phase) green[ROTATE(i)] = 255 - green[ROTATE(i)];



        if( i <= numHourPixels ){ // && i>= numMinPixels
            red[ROTATE(i)] = 255;
        } else {
            red[ROTATE(i)] = 0;
        }
        blue[ROTATE(i)] = 0;
        
    }
    // set the smoothing pixel:
    // every 5000ms equals 6 pixels, so every (5000/6)ms quals one pixel
    if(time_get_minutes()%2 == 0){
        uint16_t lvl = 1<<(8UL-millisIntoNextPixel*6*8/5000)-1;   // *6/5000 makes it into the range [0,1). We multiply my 8 to make it into the range [0,7)
        lvl = 255-millisIntoNextPixel*6*255/5000;
        green[ROTATE(numSecondPixels)] = (uint8_t)lvl;
        // os_printf("ms: %d\n\r",millisIntoNextPixel);
        // os_printf("ms: %d\n\r",millisIntoNextPixel*6*8);
        // os_printf("ms: %d\n\r",millisIntoNextPixel*6*8/5000);
        // os_printf("ms: %d\n\r",2^(millisIntoNextPixel*6*8/5000));
        // float lvl = sqrt((float)millisIntoNextPixel*6.0/5000.0);
        // green[ROTATE(numSecondPixels)] = (uint8_t)(255.0-255.0*lvl);
    }else{
        uint16_t lvl = 1<<(millisIntoNextPixel*6*8/5000)-1;
        lvl = millisIntoNextPixel*6*255/5000; // <- linear
        if(phase){
            green[ROTATE(numSecondPixels)] = 255-(uint8_t)lvl;
        }else{
            green[ROTATE(numSecondPixels)] = (uint8_t)lvl;
        }
    }
}



void ICACHE_FLASH_ATTR display_time_v4() {
    // do_measure_time();

    double minutesAngle = (double)time_get_minutes()/60 + (double)time_get_seconds()/3600 + (double)time_get_millis()/3600000;
    double secondsAngle = (double)time_get_seconds()/60 + (double)time_get_millis()/60000;
    double hoursAngle   = (double)time_get_hours()/12 + (double)time_get_minutes()/(12*60);
    // if we did not account for millis in the minutesangle it can "bump" taking over the seconds for a short time (at most a second) resulting in flashes

    // printf("%.8f --- %.8f\t\t",minutesAngle*NUMLEDS,secondsAngle*NUMLEDS);

    // clear display:
    uint8_t i;
    for(i=0;i<NUMLEDS;i++){
        red[i] = 0; green[i] = 0; blue[i] = 0;
    }


    // this variable indicates whether we are in the growing or shrinking 59 seconds cycle
    bool phase =  time_get_minutes()%2 == 0 && minutesAngle > secondsAngle
               || time_get_minutes()%2 == 1 && secondsAngle > minutesAngle;
    

    // red arc between seconds and minutes:
    if(phase){
        showArc(minutesAngle,secondsAngle,COLOR_GREEN);
    }else{
        showArc(secondsAngle,minutesAngle,COLOR_GREEN);
    }

    showArc(0,hoursAngle,COLOR_RED);


    // double millisAngle = (double)time_get_millis()/1000;
    // phase =  time_get_seconds()%2 == 0 && millisAngle > secondsAngle
    //       || time_get_seconds()%2 == 1 && secondsAngle > millisAngle;

    // if(phase){
    //     showArc(millisAngle,secondsAngle,COLOR_BLUE);
    // }else{
    //     showArc(secondsAngle,millisAngle,COLOR_BLUE);
    // }


}




// fill an arc with a color and smooth edges in a specified color
// takes care of overflow if start > stop
// start and stop are in the interval [0,1)
void ICACHE_FLASH_ATTR showArc(double start, double stop, color_t color) {
    // printf("%2.3f --- %2.3f\t\t",start*NUMLEDS,stop*NUMLEDS);
    
    // first we split into whole and partial leds
    double integral;
    double startfraction = modf(start*NUMLEDS,&integral);
    uint8_t startled = ((uint8_t)integral+1) % NUMLEDS;         // +1 to ceil 

    double stopfraction = modf(stop*NUMLEDS,&integral);
    uint8_t stopled = ((uint8_t)integral+NUMLEDS) % NUMLEDS;    // stop is floored 
    
    // the first and last led are faded:
    uint8_t fadestartled = (startled-1+NUMLEDS) % NUMLEDS;      // the led before startled is fadein
    uint8_t fadestopled  = (stopled)            % NUMLEDS;      // stopled itself is the fadeout led

    // create a mirror of the color array that we wish to work on
    // remember: arrays are pointers...
    uint8_t *colorarray;
    if(color == COLOR_RED)   colorarray = red;
    if(color == COLOR_GREEN) colorarray = green;
    if(color == COLOR_BLUE)  colorarray = blue;


    // the big portion of the arc:
    // if the start and stop point to the same led we do something else:
    if(fadestartled == fadestopled){ 
        // either all on or all off
        if(stop < start){
            uint8_t i;
            for(i=0;i<NUMLEDS;i++) {
                colorarray[i] = 255;  // this wrongly turn on the faded led itself but we'll adjust it later
            }
        }//else{
            // all off
        //}
    }else{ //
        uint8_t i;
        // turn leds on from startled upuntil (but not including) stopled:
        for(i=startled;i!=stopled;i=(i+1)%NUMLEDS) { 
            colorarray[ROTATE(i)] = 255;
        }
    }
    

    // printf("%d %d - %d %d\t\t",fadestartled,startled,stopled,fadestopled);

    uint8_t startlvl = 255 - (startfraction*255);
    uint8_t stoplvl  = stopfraction*255; 

    // printf("%d -- %d\n",startlvl,stoplvl);

    if(fadestartled == fadestopled) {
        // calcultate the outside areas of the led:
        uint16_t together = (uint16_t)startlvl + stoplvl;
        if(together > 255) together = 255;

        // if the outsides are on we simply add, but if we just want a very small arc
        // we want the remainder actually:
        if(start < stop) together = 255-together;
        colorarray[ROTATE(fadestartled)] = together;

    }else{
        colorarray[ROTATE(fadestartled)] = startlvl;
        colorarray[ROTATE(fadestopled)]  = stoplvl;
    }
}