#!/bin/bash

mkdir {singlepdf,widepdf,singlesvg,widesvg,png_1024x768,png_1280x960,png_1600x1200,png_1280x720,png_1920x1080}

for i in {1..29}
do
   gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=$i -dLastPage=$i -sOutputFile=singlepdf/page$i.pdf spacetime.pdf
   gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=$i -dLastPage=$i -sOutputFile=widepdf/wide$i.pdf widescreen.pdf
   
   pdf2svg singlepdf/page$i.pdf singlesvg/page$i.svg
   pdf2svg widepdf/wide$i.pdf widesvg/wide$i.svg
   
   convert -density 300 -resize 1024x singlepdf/page$i.pdf png_1024x768/page${i}_1024x768.png
   convert -density 300 -resize 1280x singlepdf/page$i.pdf png_1280x960/page${i}_1280x960.png
   convert -density 300 -resize 1600x singlepdf/page$i.pdf png_1600x1200/page${i}_1600x1200.png

   convert -density 300 -resize 1280x widepdf/wide$i.pdf png_1280x720/wide${i}_1280x720.png
   convert -density 300 -resize 1920x widepdf/wide$i.pdf png_1920x1080/wide${i}_1920x1080.png

done


