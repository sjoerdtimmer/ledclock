hetmismnul
mvijftienm
kwartmover
mvoorhalfm
achtweezes
drielftien
mzevenegen
viertwaalf
eenvijfuur
   ....   

total width of tekst on one line: 16.6cm door de fixed spacing van de leds

mins/5==0  het is <hour> uur
mins/5==1  het is vijf over <hour>
mins/5==2  het is tien over <hour>
mins/5==3  het is kwart over <hour>
mins/5==4  het is tien voor half <hour+1>
mins/5==5  het is vijf voor half <hour+1>
mins/5==6  het is half <hour+1>
mins/5==7  het is vijf over half <hour+1>
mins/5==8  het is tien over half <hour+1>
mins/5==9  het is kwart voor <hour+1>
mins/5==10 het is tien voor <hour+1>
mins/5==11 het is vijf voor <hour+1>

dots[0] = mins%5 > 0
dots[1] = mins%5 > 1
dots[2] = mins%5 > 2
dots[3] = mins%5 > 3
generally:
dots[i] = mins%5 > i 