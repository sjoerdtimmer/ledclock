
-- read the docs: https://readthedocs.org/projects/nodemcu/

--ws2812.write(3,string.char(0,255,0):rep(10),30)
NUMLEDS = 94

bytes = {}
for i=1,3*NUMLEDS do
	bytes[i] = 0
end


onleds = {
	87,86,85, -- het
	91,90,    -- is
	69,68,67,66,65, -- kwart
    74,73,72,71, -- over 
    30,31,32,33,34 -- negen
}
for k,i in pairs(onleds) do
	print(i)
	bytes[i*3-2] = 255
	bytes[i*3-1] = 255
	bytes[i*3-0] = 255
end

-- for k,v in pairs(bytes) do
-- 	print(k,":",v)
-- 	tmr.wdclr()
-- end

rgb = string.char(unpack(bytes))
ws2812.write(3,rgb,3*NUMLEDS)