#!/bin/python3
import svgwrite
from math import pi,ceil

# config:
thickness    = 4      # of the wood
boxlength    = 150+16 # inside length 
boxwidth     = 75     # inside width
boxheight    = 65     # inside height
radius       = 15     # radius of kerfed corners

kerfLongNum  = 6      # number of kerfs in the logitudal direction
laserwidth   = 0.35   # polulu says 0.25 but then the remaining slices of wood get way too thin

pagewidth    = 297
pageheight   = 210

forLedClock  = True   # creates some extra holes for connectors and wires

# only change for dramatically large or small boxes
# this is used where a small margin is needed
# for instance the small nubs at the bottom that 'click'
# into the slots to close the box
tinymargin   = 1

# derivative dimensions:

# center of the drawing
centerx = pagewidth/2
centery = pageheight/2

# lengths of the individual sections of the box-top
sideheight = boxheight-radius
topwidth   = boxwidth-2*radius

# kerfing parameters
innerbendlength = (pi*(radius))/2
outerbendlength = (pi*(radius+thickness))/2 # 1/4 circumference
# we added thickness to the radius to obtain the outer radius.
# since the sheet is going to bend we need enough kerfing to accomodate
# the inner as well as the outer circumference of the bend.
# this is presuming that the bending occurs mostly through
# compressing the inside, and very little through stretching the outside...

totalkerfwidth = 1.0*innerbendlength+0.0*outerbendlength

# to accomodate the bend we need to remove this amount of wood:
removewood = outerbendlength-innerbendlength
# number of kerfed lines in lateral direction,always odd, rounding up:
kerfLatNum   = 2*ceil((removewood/laserwidth)/2)-1
kerfLatSep   = totalkerfwidth/(kerfLatNum-1) # divided by the number of spaces inbetween <kerflines> lines
kerfLongSep  = 2*kerfLatSep # sep between kerfs longitidinal direction
kerflength   = (boxlength+4*thickness-(kerfLongNum+1)*kerfLongSep)/kerfLongNum # longitudal length of kerflines


# make the drawing of the top:
svg = svgwrite.Drawing(filename = "top.svg",
    size = ("%fmm"%pagewidth, "%fmm"%pageheight),
    viewBox="0 0 %f %f"%(pagewidth,pageheight)) 
    # viewBox makes the units inside paths equal mm's

# shortcut to produce mm's instead of px                       
# def mm(x,y):
# 	return ("%fmm"%x,"%fmm"%y)

# some handy shortcuts:
def mm(x):
	return "%fmm"%x

def drange(start, stop, step):
	r = start
	while r < stop:
		yield r
		r += step


cuts = svg.add(svg.g(id='cuts',stroke='red',stroke_width='0.1mm')) # group
engravings = svg.add(svg.g(id='engravings',stroke='blue',stroke_width='0.1mm')) # group




for sx in [-1,1]:
	for sy in [-1,1]:
		p = svg.path(['M',centerx,centery],fill='none')
		p.push(['m',0,sy*(-boxlength/2-2*thickness)])
		p.push(['h',sx*(-topwidth/2-totalkerfwidth-sideheight-2*thickness)])
		p.push(['v',sy*2*thickness])
		p.push(['l',sx*thickness,sy*thickness])
		# p.push(['v',sy*thickness]) # not technically necessary but this seems to look good
		p.push(['h',sx*thickness])
		p.push(['v',sy*2*thickness])
		p.push(['h',sx*(-thickness)])
		p.push(['V',centery])
		cuts.add(p)

		# square hole for bottom plate
		p = svg.path(['M',centerx,centery],fill='none')
		p.push(['m',sx*(-topwidth/2-totalkerfwidth-sideheight),sy*(-boxlength/2)])
		p.push(['h',sx*(-thickness)])
		p.push(['v',sy*(-thickness)])
		p.push(['h',sx*(thickness)])
		p.push(['v',sy*(thickness)])
		cuts.add(p)

		# rectangle holes for side plates
		p = svg.path(['M',centerx,centery],fill='none')
		p.push(['m',sx*(-topwidth/2-totalkerfwidth-sideheight+1.5*thickness),sy*(-boxlength/2)])
		p.push(['h',sx*2*thickness])
		p.push(['v',sy*(-thickness)])
		p.push(['h',sx*(-2*thickness)])
		p.push(['v',sy*thickness])
		cuts.add(p)

		p = svg.path(['M',centerx,centery],fill='none')
		p.push(['m',sx*(-topwidth/2-totalkerfwidth-1.5*thickness),sy*(-boxlength/2)])
		p.push(['h',sx*(-2*thickness)])
		p.push(['v',sy*(-thickness)])
		p.push(['h',sx*(2*thickness)])
		p.push(['v',sy*(thickness)])
		cuts.add(p)



 
# now for the kerf slots:
for sx in [-1,1]:
	for n in range(kerfLatNum):
		p = svg.path(['M',centerx,centery],fill='green')
		p.push(['m',sx*(topwidth/2+n*kerfLatSep),-boxlength/2-2*thickness])
		if n%2==1:
			for i in range(kerfLongNum):
				p.push(['m',0,kerfLongSep])
				p.push(['v',kerflength])
		else: 
			p.push(['v',(kerflength+kerfLongSep)/2])
			p.push(['m',0,kerfLongSep])
			for i in range(kerfLongNum-1):
				p.push(['v',kerflength])
				p.push(['m',0,kerfLongSep])
			p.push(['v',(kerflength+kerfLongSep)/2])
		cuts.add(p)


# print(svg.tostring())
svg.save()


####################################
##  THE BASE                      ##
####################################
svg = svgwrite.Drawing(filename = "base.svg",
    size = ("%fmm"%pagewidth, "%fmm"%pageheight),
    viewBox="0 0 %f %f"%(pagewidth,pageheight)) 


cuts = svg.add(svg.g(id='cuts',stroke='red',stroke_width='0.1mm')) # group
engravings = svg.add(svg.g(id='engravings',stroke='blue',stroke_width='0.1mm')) # group


for sx in [-1,1]:
	for sy in [-1,1]:
		p = svg.path(['M',centerx,centery],fill='none')
		p.push(['m',0,sy*(-boxlength/2-2*thickness)])
		p.push(['h',sx*(boxwidth/2)])
		p.push(['v',sy*thickness])
		p.push(['h',sx*thickness])
		p.push(['v',sy*thickness])
		p.push(['h',sx*-thickness])
		p.push(['v',sy*thickness])
		p.push(['h',sx*-2*thickness])
		p.push(['h',sx*2*thickness])
		p.push(['h',sx*thickness])
		p.push(['v',sy*-tinymargin])
		p.push(['l',sx*tinymargin,sy*tinymargin])
		p.push(['v',sy*2*tinymargin])
		p.push(['l',sx*-tinymargin,sy*tinymargin])
		p.push(['v',sy*-tinymargin])
		p.push(['h',sx*-thickness])
		p.push(['h',sx*-2*thickness])
		p.push(['l',sx*2*thickness,sy*tinymargin])
		p.push(['V',centery])
		cuts.add(p)
		

if forLedClock:
	box = engravings.add(svg.g(id='box'))

	# TODO: CC logo stuff
	# TODO: warning about 220V inside
	# TODO: arduino inside
	# TODO: other markings (network etc?)
	lines = [
		(0,"Randomdata LedClock",10),
		(1,"© 2014 Sjoerd T. Timmer",6),
		(2,"themba@randomdata.nl",6),
		(3,"http://randomdata.nl/wiki/index.php/LedClock",6),
		(4,"https://bitbucket.org/sjoerdtimmer/randomdata-ledclock",6)
	]

	for (dy,txt,size) in lines:
		# cool fonts:
		# Black Ops One		
		# Sansation Light
		# Nixie One
		txt = box.add(svg.text(txt,
                   fill = "none",
                   style = "font-size:%spx; font-family:Nixie One"%size))
		txt.rotate(-90)
		txt.translate(0,dy*11-20)

	box.translate(centerx,centery+boxlength/2+thickness)

	# engravings.add(svg.image('logos/by.svg',(centerx,centery),(50,50)))
	# engravings.add(txt)
	# svg.add(svg.image("top.svg",(0,0),(pagewidth,pageheight)))
	
	# engravings.add(svg.text(copyrightstr,(centerx,centery),lengthAmplete file are to be rendered into a given rectangle within the current user coordinate system. The image element can refer to raster image files such as PNG or JPEdjust='spacingAndGlyphs'))
	# pass

svg.save()		


##############################
##  THE SIDES               ##
##############################

svg = svgwrite.Drawing(filename = "side.svg",
    size = ("%fmm"%pagewidth, "%fmm"%pageheight),
    viewBox="0 0 %f %f"%(pagewidth,pageheight)) 


cuts = svg.add(svg.g(id='cuts',stroke='red',stroke_width='0.1mm')) # group
engravings = svg.add(svg.g(id='engravings',stroke='blue',stroke_width='0.1mm')) # group

for sx in [-1,1]:
	p = svg.path(['M',centerx+sx*(boxwidth/2+2*thickness),centery],fill='none')
	# p.push(['m',0,boxheight/2])
	p.push(['h',boxwidth/2])
	p.push(['v',-1.5*thickness])
	p.push(['h',thickness])
	p.push(['v',-2*thickness])
	p.push(['h',-thickness])
	p.push(['v',-boxheight+radius+7*thickness])
	p.push(['h',thickness])
	p.push(['v',-2*thickness])
	p.push(['h',-thickness])
	p.push(['v',-1.5*thickness])
	p.push_arc(target=(-radius,-radius),rotation=90, r=radius,large_arc=False,angle_dir='-')
	p.push(['h',-topwidth])
	p.push_arc(target=(-radius,radius),rotation=90, r=radius,large_arc=False,angle_dir='-')
	p.push(['v',1.5*thickness])
	p.push(['h',-thickness])
	p.push(['v',2*thickness])
	p.push(['h',thickness])
	p.push(['v',boxheight-radius-7*thickness])
	p.push(['h',-thickness])
	p.push(['v',2*thickness])
	p.push(['h',thickness])
	p.push(['v',1.5*thickness])
	p.push(['h',boxwidth/2])
	cuts.add(p)


if forLedClock:
	powerradius = 3
	cableradius = 1.5
	ethwidth = 16.9
	ethheight = 14.4
	ethpcbthickness = 1.1
	ethpcbwidth = 34
	ethpcblength = 56
	screwhole = 1
	barwidth = 4
	cuts.add(svg.circle((centerx+boxwidth+2*thickness-powerradius-thickness,centery-powerradius-thickness),powerradius,fill='none'))
	cuts.add(svg.circle((centerx+boxwidth+2*thickness-cableradius-thickness,centery-boxheight/2-cableradius),cableradius,fill='none'))
	cuts.add(svg.rect((centerx+ethpcbwidth/2-ethwidth/2+2*thickness,centery+-thickness-ethheight-ethpcbthickness),(ethwidth,ethheight),fill='none'))	

	p = svg.path(['M',centerx+2*thickness,centery+thickness],fill='none')
	p.push(['h',boxwidth])
	p.push(['v',ethpcblength/2+barwidth])
	p.push(['h',-boxwidth+ethpcbwidth+12])
	p.push(['l',-12,12,])
	p.push(['v',ethpcblength/2-12])
	p.push(['h',-ethpcbwidth])
	p.push(['v',-8])
	p.push(['h',4])
	p.push(['l',4,4])
	p.push(['h',ethpcbwidth-16])
	p.push(['l',4,-4])
	p.push(['v',-ethpcblength+8+8])
	p.push(['l',-8,-8])
	p.push(['h',-ethpcbwidth+12+12])
	p.push(['l',-8,8])
	p.push(['h',-4])
	p.push(['v',-8-barwidth])
	# p.push(['v',-ethpcblength-barwidth])
	# p.push(['',])
	cuts.add(p)

	for sx in [-27.2/2,27.2/2]:
		for sy in [-48.1/2,48.1/2]:
			cuts.add(svg.circle((centerx+sx+2*thickness+ethpcbwidth/2,centery+sy+thickness+barwidth+ethpcblength/2),screwhole,fill='none'))



svg.save()

