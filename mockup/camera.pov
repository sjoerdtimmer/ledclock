// declares positon and view directions
// Generated by FreeCAD (http://free-cad.sf.net/)

// Total number of camera positions
#declare nCamPos = 1;

// Array of positions
#declare  CamPos = array[1] {
   <77.6379,114.71,23.8168>,
};
// Array of Directions (only for special calculations)
#declare  CamDir = array[1] {
   <0.00562274,-0.992619,-0.121141>,
};
// Array of Look At positions
#declare  LookAt = array[1] {
   <78.2903,-0.461967,9.76102>,
};
// // Array of up vectors
#declare  Up = array[1] {
   <-0.0460226,-0.121271,0.991552>,
};
// // Array of up vectors
#declare  CamZoom = array[1] {
   45,
};

