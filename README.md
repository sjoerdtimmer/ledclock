# Build instructions #

This project has been under quite heavy development lately (at the time of writing). The wiki (http://randomdata.nl/wiki/index.php?title=LedClock) contains details about the design and the development process but I thought it would be a great idea to have some final build instructions. Here they are:

### Requirements/parts: ###
These are the parts for a **completely redesigned** version of the clock. The appearance is still exactly the same but the controller is now an esp8266 wifi module that fits inside the ring of the clock. (the old version used an arduino and an ethernet module).

There are a few links to eBay on the wiki but they tend to change. Give it a quick search and you'll probably find similar items for similar prices. 

* Leds. This is probably the most expensive part. My designs are all built around 50 cm @ 144 leds/m = 72 leds. These are relatively expensive since the leds are really close together but it does give very nice results. Mine cost about $21 when I started this project, but nowadays they sell for as little as $13. Most are advertised as ws2812 but are actually ws2811.
* 5V power supply. This needs to be rather hefty. I used a 5A version because I wanted to be really sure that even if I accidentally turned all leds full white it wouldn't break. If you are feeling brave you could probably calculate the maximum number of leds that can be on at a time and choose a slightly smaller supply but that's up to you.
* a small pcb (see pcb folder for design). Can be ordered at oshpark for slightly over $5.
* esp8266 (v1 module) (~$3)
* power cord (my supply didn't come with one)
* 1000uF capacitor to smooth the 5v line (the ledstrip can create big fluctuations that the esp really doesn't like). 
* 22uF capacitor
* 100 ohm resistor
* AMS1117 3v3 regulator
* a cable (white looks good) I re-purposed a 5ft usb cable.
* 3D printed frame. Look in the 3D design section of this repo and you'll find freecad designs as well as the corresponding stl and gcode. Make sure to print the latest version (v6 at the time of writing). In White PLA it looks quite good because the light shines through.


### Assembly: ###
There is a cavity in the underside of the frame. This can be used to accommodate all electronics. 
The pcb has too many components. At first I thought a logic level shifter would be required but it turns out the 3v3 signal is okay for the ledstrip. Also, adafruit recommends the use of a protection resistor. I mistakenly added one to the pcb (470 ohm) but it later turned out that to be effective it needs to be placed at the ledstrip side of the transmission line. Also, in combination with the logic level converter it is redundant and degrades the signal quality (to an unreliable level that sometimes does and sometimes doesn't work) because it is supposed to compensate for the low-impedance output signal of the controller, but the logic level converter has a relatively high-impedance output...

TODO: add pictures


### firmware ###
This repo contains my code to control the clock. 
The firmware is flashed in two stages:
* at the hardware level, we use nodemcu(written in plain c, compiled against headers for the chip on the esp)
* nodemcu executes lua code which is stored in a simple filesystem on the system flash

```
esptool.py write_flash 0x00000 <nodemcu bin file>
```

At this point you may want to fire up your favourite serial console application to check that you can get a terminal:
```
screen /dev/ttyUSB0 9600
>
```

```
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 node format
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload clock.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload hsv.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload response.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload utils.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload weather.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload web.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0 --baud 9600 upload ini.lua:init.lua --restart
```


# Using the clock #
When the clock boots up it shows a rotating rainbow until is has successfully connected to the wifi AP and obtained a timestamp through NTP. If you see the rainbow at any other time, it means the firmware has crashed and the clock is rebooting. 

### Configuration ###
If this is the first time you use the clock it will not know which AP to connect to and the rainbow will keep showing untill you configure the wifi settings. To do so, connect to the `LedClock` wifi with password `NaeVaep3fu`, and visit `http://192.168.4.1` in your browser. For some reason, visiting this with my desktop made the clock crash but I can successfully configure the clock using my android phone. (I presume that my desktop chrome is sending too much metadata with the request that is buffered in ram in the module, resulting in a ram overflow issue of some kind.)

Although the webinterface suggests that you can set the timezone and location, this is not actually true. This needs to be done in the lua code!


### Reading the time ###
The clock uses only two colors: red and green. When mixed, they form yellow. The hours are shown in green by means of a green arc that starts at 12 o'clock and ends at the current location of the hours-arm.

The minutes and seconds are indicated using red pixels. To know what time it is you have to find the crossovers black/red or yellow/green. One of these is moving, so that is the seconds-arm. The remaining black/red or yellow/green crossing is the minutes-arm.

### Reading the weather ###
Once every 5 minutes the clock will fetch a weather update for the hardcoded location. This is from a service for dutch weather forecasts. To get weather updates for other countries, you're on your own for now. To warn you of a weather forecast a red led will blink 5 times, after which the forecast for the next 60 minutes is plotted along the perimeter of the clock (just right of the red led is the forecast for a few minutes from now and just left of it is the forecast for 59 minutes from now). If the forecast contains no >0 values the display routine is skipped. 




### Who do I talk to? ###
Any assistance will be gladly provided. If you need help sourcing parts, have questions about the code, the printing or anything else, please feel free to contact me:
* themba@randomdata.nl
* themba on #randomdata on freenode
There is also some more info and pictures at the [wiki](http://randomdata.nl/wiki/index.php/LedClock)